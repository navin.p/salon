//
//  GlimpsesofGalleryVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/6/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class GlimpsesofGalleryVC: UIViewController {
    //MARK: --------------IBOutlet-----------

      @IBOutlet weak var collection_DashBoard: UICollectionView!
      @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
      var ary_CollectionData = NSMutableArray()
 
    //MARK: --------------LifeCycle------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = false
        if(nsud.value(forKey: "SalonGalleryData") != nil){
            ary_CollectionData = (nsud.value(forKey: "SalonGalleryData")as! NSArray).mutableCopy()as! NSMutableArray
            self.activityIndicator.isHidden = true
        }
    }
    // MARK: - ------IBAction----------
       // MARK: -
       @IBAction func actionBack(_ sender: UIButton) {
           scaleAnimationONUIButton(sender: sender)
           
           self.navigationController?.popViewController(animated: true)
       }

}
// MARK: - ----------------UICollectionViewDelegate
//CALayer -

extension GlimpsesofGalleryVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ary_CollectionData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "galleryCell", for: indexPath as IndexPath) as! galleryCell
        let urlImage = "\(ary_CollectionData.object(at: indexPath.row))"
        cell.gallery_Image.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), usingActivityIndicatorStyle: .gray)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collection_DashBoard.frame.size.width)  / 2 - 8, height:(self.collection_DashBoard.frame.size.width)  / 2 - 5 )
      
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
 
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let urlImage = "\(ary_CollectionData.object(at: indexPath.row))"
        let img = UIImageView()
        img.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "salon_on_wheels"), options: SDWebImageOptions(rawValue: 0), progress: { (S1, S2) in
        }, completed: { (IMAGE, ERROR, cacheType, imageURL) in
            let testController = mainStoryboard.instantiateViewController(withIdentifier: "PreviewImageVC")as! PreviewImageVC
            testController.img = IMAGE!
            self.navigationController?.pushViewController(testController, animated: true)
        }, usingActivityIndicatorStyle: .gray)
        
       
    }
    
}

class galleryCell: UICollectionViewCell {
    //galleryCell
    @IBOutlet weak var gallery_Image: UIImageView!
}

//
//  ServiceSubListVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/7/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class ServiceSubListVC: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lbltitle: UILabel!

    var aryList = NSMutableArray()
    var refreshControl = UIRefreshControl()
    var dictData = NSMutableDictionary()
    // MARK: - ------LifeCycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        tv_List.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
         refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
         tv_List.addSubview(refreshControl)
        
        lbltitle.text = "\(dictData.value(forKey: "name")!)"
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true

        if(aryList.count == 0){
             self.ServicesubListAPI()
        }else{
            self.tv_List.reloadData()
            self.tv_List.delegate = self
            self.tv_List.dataSource = self
        }
    }
    @objc func refresh(_ sender: AnyObject) {
        self.ServicesubListAPI()
    }

    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        
        self.navigationController?.popViewController(animated: true)
        
    }

}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ServiceSubListVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "OfferCell" : "OfferCell", for: indexPath as IndexPath) as! OfferCell
        let dict = ((aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary).removeNullFromDict()

        cell.lblTitle.text = "\(dict.value(forKey: "title")!)"
        cell.lblDisAmmount.text = "\u{20B9}\(dict.value(forKey: "regular_price")!)"
        cell.lblAmmount.text = "\u{20B9}\(dict.value(forKey: "price")!)"
        
        if("\(dict.value(forKey: "regular_price")!)" == ""){
            cell.lblDisAmmount.text = ""
        }
        if("\(dict.value(forKey: "regular_price")!)" == "\(dict.value(forKey: "price")!)"){
            cell.lblDisAmmount.text = ""
        }
       
        
        cell.btnADDNOW.isHidden = true
        if(globleLogInData.count != 0){
            let role = "\(globleLogInData.value(forKey: "role")!)"
            if(role == "customer"){
                cell.btnADDNOW.isHidden = false
            }else{
                cell.btnADDNOW.isHidden = true
            }
        }
    
        cell.btnADDNOW.tag = indexPath.row
        cell.btnADDNOW.addTarget(self, action: #selector(unFollowAction), for: .touchUpInside)
        cell.imgView.image = UIImage(named: "salon_on_wheels")
        cell.imgView.layer.cornerRadius =  6.0
       
        cell.imgView.layer.masksToBounds = false
        cell.imgView.clipsToBounds = true
        if(dict.value(forKey: "images") is NSArray){
            let aryImage = dict.value(forKey: "images") as! NSArray
            if(aryImage.count != 0){
                let urlImage = "\(aryImage.object(at: 0))"
                cell.imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "salon_on_wheels"), options: SDWebImageOptions(rawValue: 0), progress: { (S1, S2) in
                }, completed: { (IMAGE, ERROR, cacheType, imageURL) in
                    
                }, usingActivityIndicatorStyle: .gray)
            }
        }
     
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = ((aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary).removeNullFromDict()
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ServiceDetailVC") as? ServiceDetailVC
            vc?.dictData = dict.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc!, animated: true)
      
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryList.count == 0 ? 100 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
    @objc func unFollowAction(sender : UIButton) {
        if(globleLogInData.count == 0){
            let alert = UIAlertController(title: alertMsg, message: alertLogin, preferredStyle: UIAlertController.Style.alert)
            alert.view.tintColor = UIColor.black
            alert.addAction(UIAlertAction (title: "LOGIN", style: .default, handler: { (nil) in
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }))
            alert.addAction(UIAlertAction (title: "CLOSE", style: .default, handler: { (nil) in
                
               // self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }else{
            let dict = aryList.object(at: sender.tag)as! NSDictionary
            
            ServiceAddTocartAPI(dict: dict.mutableCopy()as! NSMutableDictionary)
        }
      
    }
}

// MARK: -
// MARK: - ---------------API Calling----------

extension ServiceSubListVC  {
    func ServicesubListAPI() {
        if(isInternetAvailable()){
         self.activityIndicator.isHidden = false
            let dictdata = NSMutableDictionary()
         
            dictdata.setValue("\(dictData.value(forKey: "term_id")!)", forKey: "cat_id")
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_get_product_by_cat) { (responce, status) in
              self.activityIndicator.isHidden = true
                self.refreshControl.endRefreshing()
 print(responce)
                if (status){
                    self.aryList = NSMutableArray()
                    self.aryList = ((responce as NSDictionary).value(forKey: "data")as! NSArray).mutableCopy() as! NSMutableArray
                    self.tv_List.reloadData()
                    self.tv_List.delegate = self
                    self.tv_List.dataSource = self
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func ServiceAddTocartAPI(dict : NSMutableDictionary) {
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)

            let dictdata = NSMutableDictionary()

            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue("\(dict.value(forKey: "id")!)", forKey: "pid")
            dictdata.setValue("0", forKey: "variation_id")
            dictdata.setValue("1", forKey: "quantity")
        
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_add_cart) { (responce, status) in
                print(responce)
                loader.dismiss(animated: false, completion: nil)

                if (status){
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    if("\(dict.value(forKey: "error")!)" == "1"){
                       showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        let alert = UIAlertController(title: alertInfo, message: "\(dict.value(forKey: "message")!)", preferredStyle: UIAlertController.Style.alert)
                        alert.view.tintColor = UIColor.black
                        // add the actions (buttons)
                        alert.addAction(UIAlertAction (title: "Go to Appointment", style: .default, handler: { (nil) in
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AppoinmentVC") as? AppoinmentVC
                            self.navigationController?.pushViewController(vc!, animated: true)
                        }))
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                           // self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)
                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
}

//
//  ForgotPassVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/6/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController {
    //MARK: --------------IBOutlet-----------
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    //MARK: --------------LifeCycle------------
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //MARK: --------------Extra------------

    @objc func textFieldDidChange(_ textField: UITextField) {
        let text = textField.text ?? ""
        var trimmedText = text.trimmingCharacters(in: .whitespaces)
        trimmedText = (trimmedText.replacingOccurrences(of: " ", with: ""))
        
        textField.text = trimmedText
        if(txtMobileNumber == textField){
            textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
        }
    }
    //MARK: --------------IBAction------------

    @IBAction func actionONSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        scaleAnimationONUIButton(sender: sender)
        if(validationForforgot()){
            self.ForgotPasswordAPI()
        }
    }
    
 
    @IBAction func actionBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.navigationController?.popViewController(animated: true)
    }
   

}
// MARK:
// MARK:- Validation

extension ForgotPassVC {
    func validationForforgot() -> Bool {
        
        if(txtMobileNumber.text?.count == 0){
            addErrorMessage(textView: txtMobileNumber, message: alert_MobileNumber)
            return false
        }else if(txtMobileNumber.text!.count < 10){
            addErrorMessage(textView: txtMobileNumber, message: alert_MobileNumberValid)
            return false
            
        }
        return true
    }
   
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- -------------UITextFieldDelegate----------

extension ForgotPassVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtMobileNumber){
            return TextValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        else {
            return TextValidation(textField: textField, string: string, returnOnly: "", limitValue: 45)
        }
    }
}
// MARK: -
// MARK: - API Calling

extension ForgotPassVC  {
    func ForgotPasswordAPI() {
        if(isInternetAvailable()){
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(txtMobileNumber.text!)", forKey: "user_phone")
            
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_ForgotPAss) { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                        if (status){
                            let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            
                            print(dict)
                            if("\(dict.value(forKey: "error")!)" == "1"){
                               showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                            }else{
                                let alert = UIAlertController(title: "Success", message: "\(dict.value(forKey: "message")!)", preferredStyle: UIAlertController.Style.alert)
                                alert.view.tintColor = UIColor.black
                                // add the actions (buttons)
                                alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                    self.navigationController?.popViewController(animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                                
                             

                               
                            }
                     
                        }else{
                            showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)


                        }
                    }
            //
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
}

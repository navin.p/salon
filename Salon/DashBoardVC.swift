//
//  DashBoardVC.swift
//  Salon
//r on 10/5/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit
import Alamofire
class DashBoardVC: UIViewController {
    
    //MARK: --------------IBOutlet-----------

   @IBOutlet weak var viewPager: CPImageSlider!
    @IBOutlet weak var heightviewPager: NSLayoutConstraint!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var collection_DashBoard: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var arraySlider = NSMutableArray()
  var ary_SliderData = NSMutableArray()
    var ary_CollectionData = NSMutableArray()

    //MARK: --------------LifeCycle------------

    override func viewDidLoad() {
        super.viewDidLoad()
        if(DeviceType.IS_IPHONE_X){
            heightviewPager.constant = 210.0
        }else if(DeviceType.IS_IPHONE_6P){
            heightviewPager.constant = 185.0
        }else{
            heightviewPager.constant = 150.0
        }
        self.activityIndicator.isHidden = true

       
        if(nsud.value(forKey: "Salon_LoginData") != nil){
            globleLogInData = NSMutableDictionary()
            globleLogInData = (nsud.value(forKey: "Salon_LoginData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
            
        }
        PushNotiManagerVC().registerForPushNotifications()
        if(nsud.value(forKey: "fcmToken") != nil){
            fcm_token = "\(nsud.value(forKey: "fcmToken")!)"
        }
        
        if(globleLogInData.count != 0){
            let role = "\(globleLogInData.value(forKey: "role")!)"
            if(role == "administrator"){
                check_user_deviceAPI()
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(ary_CollectionData.count == 0){
            
             self.DashBoardAPI()
        }else{
            self.collection_DashBoard.reloadData()
            self.setSliderImages(aryimageData: self.ary_SliderData)
        }
        self.setContainSizeForScroll()
        if(globleLogInData.count != 0){
            self.LoginAPI()
             
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              }
    }
  
    override func viewDidDisappear(_ animated: Bool) {
        viewPager.stopAutoPlay()
    }
    override func viewWillLayoutSubviews() {
        setSliderImages(aryimageData: ary_SliderData)
   
    }
    func setSliderImages(aryimageData : NSMutableArray) {
        viewPager.stopAutoPlay()

        let aryTemp = NSMutableArray()
        for item in aryimageData {
            let dict = NSMutableDictionary()
            dict.setValue("\(item)", forKey: "image_path_sys")
            aryTemp.add(dict)
        }
  
         viewPager.images =  aryTemp
         viewPager.delegate = self
         viewPager.autoSrcollEnabled = true
         viewPager.enableArrowIndicator = false
         viewPager.enablePageIndicator = false
         viewPager.enableSwipe = true
     }
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnMenu(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        
        let nextVc: DrawerVC = mainStoryboard.instantiateViewController(withIdentifier: "DrawerVC") as! DrawerVC
        nextVc.handleDrawerView = self
        nextVc.modalPresentationStyle = .overCurrentContext
        nextVc.modalTransitionStyle = .crossDissolve
        self.present(nextVc, animated: false, completion: nil)
        
    }
    @IBAction func actionOnBottomButton(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.view.endEditing(true)
        var count = 0
        if(sender.tag == 0){ // Service
          
        }
        else if(sender.tag == 1){ //Offer
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: Offervc.self) {
                    count = 1
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            if(count == 0){
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "Offervc") as? Offervc
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
        else if(sender.tag == 2){ //Appoinment
            if(globleLogInData.count == 0){
                let alert = UIAlertController(title: alertMsg, message: alertLogin, preferredStyle: UIAlertController.Style.alert)
                alert.view.tintColor = UIColor.black
                alert.addAction(UIAlertAction (title: "LOGIN", style: .default, handler: { (nil) in
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                    self.navigationController?.pushViewController(vc!, animated: true)
                }))
                alert.addAction(UIAlertAction (title: "CLOSE", style: .default, handler: { (nil) in
                    
                   // self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
            
            
            
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: AppoinmentVC.self) {
                    count = 1
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            if(count == 0){
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AppoinmentVC") as? AppoinmentVC
                self.navigationController?.pushViewController(vc!, animated: false)
            }
                
            }
        }
        else if(sender.tag == 3){ //Profile
            if(globleLogInData.count == 0){
                let alert = UIAlertController(title: alertMsg, message: alertLogin, preferredStyle: UIAlertController.Style.alert)
                alert.view.tintColor = UIColor.black
                alert.addAction(UIAlertAction (title: "LOGIN", style: .default, handler: { (nil) in
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                    self.navigationController?.pushViewController(vc!, animated: true)
                }))
                alert.addAction(UIAlertAction (title: "CLOSE", style: .default, handler: { (nil) in
                    
                   // self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: MyProfileVC.self) {
                        count = 1
                        
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                if(count == 0){
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
            }
          
        }
        
    }

    func setContainSizeForScroll()  {
        var height = CGFloat()
        
        height = ((self.collection_DashBoard.frame.size.width / 2) + 10) * CGFloat((self.ary_CollectionData.count % 2) == 0 ? self.ary_CollectionData.count/2 : (self.ary_CollectionData.count/2)+1)
        self.heightCollectionView.constant = CGFloat(height)
          
        var str_containSize = CGFloat()
        str_containSize = self.heightCollectionView.constant + 125.0 + self.heightviewPager.constant
        self.scrollView.contentSize = CGSize(width: self.view.frame.width , height: str_containSize)

    }
    
}
// MARK: - ----------------CPSliderDelegate
// MARK: -
extension  DashBoardVC  : CPSliderDelegate{
    func sliderImageIndex(slider: CPImageSlider, index: Int) {
       
    }
    
    func sliderImageTapped(slider: CPImageSlider, index: Int) {
        
    }
}
// MARK: - ----------------UICollectionViewDelegate
//CALayer -

extension DashBoardVC : UICollectionViewDelegate ,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    private func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ary_CollectionData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DashboardCell", for: indexPath as IndexPath) as! DashboardCell
        
        let dict = ary_CollectionData.object(at: indexPath.row)as! NSDictionary
        cell.dashBoard_lbl_Title.text = "\(dict.value(forKey: "name")!)"
        let urlImage = "\(dict.value(forKey: "img_url")!)"
        cell.dashBoard_Image.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: ""), usingActivityIndicatorStyle: .gray)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collection_DashBoard.frame.size.width)  / 2 - 10, height:(self.collection_DashBoard.frame.size.width)  / 2 - 10 )
      
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0.4
        cell.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.transform = .identity
        }
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeader", for: indexPath) as? SectionHeader{
            //sectionHeader.sectionHeaderlabel.text = "Section \(indexPath.section)"
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.endEditing(true)
        
        let dict = ary_CollectionData.object(at: indexPath.row)as! NSDictionary
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ServiceSubListVC") as? ServiceSubListVC
        vc?.dictData = dict.mutableCopy()as! NSMutableDictionary
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
}

class DashboardCell: UICollectionViewCell {
    //DashBoard
    @IBOutlet weak var dashBoard_Image: UIImageView!
    @IBOutlet weak var dashBoard_lbl_Title: UILabel!
}
class SectionHeader: UICollectionReusableView {
    //@IBOutlet weak var sectionHeaderlabel: UILabel!
}
// MARK: - ------------DrawerScreenDelegate---------
// MARK: -
extension DashBoardVC: DrawerScreenDelegate {
    func refreshDrawerScreen(strType: String, tag: Int) {
        print(strType)
            if(strType == "Sign out"){
                       self.view.endEditing(true)
                       let alert = UIAlertController(title: "Sign out", message: alertLogout, preferredStyle: .alert)
                       alert.view.tintColor = UIColor.black
                       let Yes = (UIAlertAction(title: "Yes", style: .default , handler:{ (UIAlertAction)in
                           self.LogOutAPI()
                       }))
                       alert.addAction(Yes)
                   
                       alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                           print("User click Add Cancel  button")
                       }))
                       
                       
                       if let popoverController = alert.popoverPresentationController {
                           popoverController.sourceView = self.view
                           popoverController.sourceRect = self.view.bounds
                           popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                       }
                       
                       self.present(alert, animated: true, completion: {
                           print("completion block")
                       })
                       
                   }
                   
                   
                   else if (strType == "Change Password"){
                       self.view.endEditing(true)
                           let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC
                           self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Feedback"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "FeedBackVC") as? FeedBackVC
                self.navigationController?.pushViewController(vc!, animated: true)
        }
        else if (strType == "About Us"){
                      self.view.endEditing(true)
                      let vc = mainStoryboard.instantiateViewController(withIdentifier: "AboutUSVC") as? AboutUSVC
                      self.navigationController?.pushViewController(vc!, animated: true)
              }
        else if (strType == "Contact Us"){
                      self.view.endEditing(true)
                      let vc = mainStoryboard.instantiateViewController(withIdentifier: "ContactUSVC") as? ContactUSVC
                      self.navigationController?.pushViewController(vc!, animated: true)
              }
        
        else if (strType == "Assigned Booking List"){
                            self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AssignedBookingListVC") as? AssignedBookingListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Glimpses of Gallery"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "GlimpsesofGalleryVC") as? GlimpsesofGalleryVC
                self.navigationController?.pushViewController(vc!, animated: true)
        }
            else if (strType == "History"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryVC") as? HistoryVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }  else if (strType == "Review"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ReviewListVC") as? ReviewListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }  else if (strType == "Appointment List"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentListVC") as? AppointmentListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Staff List"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "StaffListVC") as? StaffListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Profile"){ //Profile
                var count = 0
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: MyProfileVC.self) {
                        count = 1
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                if(count == 0){
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
            }
            else if (strType == "Home"){
                self.view.endEditing(true)
                var count = 0
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: DashBoardVC.self) {
                        count = 1
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                if(count == 0){
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
         
            }
            else if (strType == "Login"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                vc!.view.tag = 1

                self.navigationController?.pushViewController(vc!, animated: false)
         
            }
            else if (strType == "Signup"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                vc!.view.tag = 2

                self.navigationController?.pushViewController(vc!, animated: false)
         
            }
    }
    
    
    
}
// MARK: -
// MARK: - API Calling

extension DashBoardVC  {

    func DashBoardAPI() {
        if(isInternetAvailable()){
            self.activityIndicator.isHidden = false
            let dictdata = NSMutableDictionary()
            WebService.callAPIBYGET(parameter: dictdata, url: URL_DashBoard) { (responce, status) in
                self.activityIndicator.isHidden = true
                
                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    if(dict.value(forKey: "sliders") != nil){
                        self.ary_SliderData = NSMutableArray()
                        self.ary_SliderData = (dict.value(forKey: "sliders")as! NSArray).mutableCopy() as! NSMutableArray
                    }
                    if(dict.value(forKey: "sliders") != nil){
                        self.ary_CollectionData = NSMutableArray()
                        self.ary_CollectionData = (dict.value(forKey: "services")as! NSArray).mutableCopy() as! NSMutableArray
                    }
                    if(dict.value(forKey: "gallery") != nil){
                        nsud.setValue((dict.value(forKey: "gallery")as! NSArray), forKey: "SalonGalleryData")

                    }
                    self.setContainSizeForScroll()
                    self.collection_DashBoard.reloadData()
                    self.setSliderImages(aryimageData:  self.ary_SliderData)
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func LogOutAPI() {
         if(isInternetAvailable()){
             let dictdata = NSMutableDictionary()
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)

            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
             WebService.callAPIBYPOST(parameter: dictdata, url: URL_LogOut) { (responce, status) in
                 if (status){
                    loader.dismiss(animated: false, completion: nil)
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary

                    
                    if("\(dict.value(forKey: "error")!)" == "1"){
                        showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        UserDefaults.standard.synchronize()
                        globleLogInData = NSMutableDictionary()
                        self.view.endEditing(true)
                        let redViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as! LoginSigUpVC
                        let navigationController = UINavigationController(rootViewController: redViewController)

                        UIApplication.shared.windows.first?.rootViewController = navigationController
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
                        navigationController.setNavigationBarHidden(true, animated: true)
                    }
                    
                 }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                 }
             }
         }
         else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
         }
     }
    
    func LoginAPI() {
        if(isInternetAvailable()){
            let dictdata = NSMutableDictionary()
            print(globleLogInData)
            dictdata.setValue("\(globleLogInData.value(forKey: "user_phone")!)", forKey: "u_phone")
            dictdata.setValue("\(globleLogInData.value(forKey: "password")!)", forKey: "u_pass")
            dictdata.setValue(str_DeviceID, forKey: "device_id")
            dictdata.setValue(fcm_token, forKey: "device_token")
            dictdata.setValue(platform, forKey: "platform")
            
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_Login) { (responce, status) in
                        if (status){
                            let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            print(dict)
                            if("\(dict.value(forKey: "error")!)" == "1"){
                                let alert = UIAlertController(title: "Failed", message: "\(dict.value(forKey: "message")!)", preferredStyle: UIAlertController.Style.alert)
                                alert.view.tintColor = UIColor.black
                                // add the actions (buttons)
                                alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                    UserDefaults.standard.synchronize()
                                    
                                    self.view.endEditing(true)
                                    let redViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as! LoginSigUpVC
                                    let navigationController = UINavigationController(rootViewController: redViewController)

                                    UIApplication.shared.windows.first?.rootViewController = navigationController
                                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                                    navigationController.setNavigationBarHidden(true, animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                          
                            }else{
                                let loginData = (dict.value(forKey: "response")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                
                                loginData.setValue(globleLogInData.value(forKey: "Remember"), forKey: "Remember")
                                loginData.setValue("\(dict.value(forKey: "role")!)", forKey: "role")
                                loginData.setValue("\(globleLogInData.value(forKey: "password")!)", forKey: "password")
                                    nsud.setValue(loginData, forKey: "Salon_LoginData")
                                    nsud.synchronize()
                                globleLogInData = NSMutableDictionary()
                                globleLogInData = (nsud.value(forKey: "Salon_LoginData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            }
                        }else{
                            showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)
                        }
               }
        }
        
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)

        }
    }
    
    func check_user_deviceAPI() {
         if(isInternetAvailable()){
            
            let dictdata = NSMutableDictionary()
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue(str_DeviceID, forKey: "device_id")
          
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_check_user_device) { (responce, status) in
                print(responce)
                
                    loader.dismiss(animated: false, completion: nil)
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    
                    if("\(dict.value(forKey: "error")!)" == "1"){
                        showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "Message")!)", viewcontrol: self)
                    }else{
                      
                    }
                    
                 }
             }
         }
       
     }
    


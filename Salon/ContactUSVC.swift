//
//  ContactUSVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/15/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit
import SafariServices

class ContactUSVC: UIViewController ,SFSafariViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    
       // MARK: - ------IBAction----------
          // MARK: -
          @IBAction func actionBack(_ sender: UIButton) {
              scaleAnimationONUIButton(sender: sender)
              self.navigationController?.popViewController(animated: true)
          }
          
     
          @IBAction func actionCall(_ sender: UIButton) {
              scaleAnimationONUIButton(sender: sender)
            if(callingFunction(number: CallNumber as NSString)){
                
            }
          }
   
          @IBAction func actionInsta(_ sender: UIButton) {
              scaleAnimationONUIButton(sender: sender)
            let url = URL(string: InstagramLink)!
            let controller = SFSafariViewController(url: url)
            self.present(controller, animated: true, completion: nil)
            controller.delegate = self
          }
    
          @IBAction func actionFB(_ sender: UIButton) {
              scaleAnimationONUIButton(sender: sender)
            let url = URL(string: FacebookLink)!
            let controller = SFSafariViewController(url: url)
            self.present(controller, animated: true, completion: nil)
            controller.delegate = self
          }
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

//
//  AppintmentListVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/9/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class AppointmentListVC: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_List: UITableView!

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var viewStaff: UIView!
    @IBOutlet weak var tv_StaffList: UITableView!
    @IBOutlet weak var activityIndicatorStaff: UIActivityIndicatorView!
     var orderID = ""
    var refreshControl = UIRefreshControl()
    var aryList = NSMutableArray(),aryStaffList = NSMutableArray(), aryProcessing = NSMutableArray() , aryAssigned = NSMutableArray() , aryCompleted = NSMutableArray()

    // MARK: - ------LifeCycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        tv_List.tableFooterView = UIView()
        tv_StaffList.tableFooterView = UIView()

        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tv_List.addSubview(refreshControl)
        segmentedControl.addTarget(self, action: #selector(self.indexChanged(_:)), for: .valueChanged)
        tv_StaffList.layer.cornerRadius = 16.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true
        self.activityIndicatorStaff.isHidden = true

        if(aryList.count == 0){
            self.AppointmentListAPI(forLoader: 0)
        }else{
           
                self.tv_List.reloadData()
                self.tv_List.delegate = self
                self.tv_List.dataSource = self
        }
    }
    @objc func refresh(_ sender: AnyObject) {
        self.AppointmentListAPI(forLoader: 0)
    }
    @objc func indexChanged(_ sender: UISegmentedControl) {
        reloadTable()
    }
    func reloadTable() {
        if segmentedControl.selectedSegmentIndex == 0 {
            self.aryList = NSMutableArray()
            self.aryList = self.aryProcessing
            self.tv_List.reloadData()
            print("Select 0")
        } else if segmentedControl.selectedSegmentIndex == 1 {
            print("Select 1")
            self.aryList = NSMutableArray()
            self.aryList = self.aryAssigned
            self.tv_List.reloadData()
        } else if segmentedControl.selectedSegmentIndex == 2 {
            print("Select 2")
            self.aryList = NSMutableArray()
            self.aryList = self.aryCompleted
            self.tv_List.reloadData()
        }
      

        self.tv_List.delegate = self
        self.tv_List.dataSource = self
        self.tv_List.reloadData()
        
    }
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionOnClose(_ sender: UIButton) {
        self.viewStaff.removeFromSuperview()

      
    }
}
// MARK: - ------UITableViewDelegate----------
// MARK: -
extension AppointmentListVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.tv_List == tableView){
            return self.aryList.count
        }else{
            return self.aryStaffList.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(self.tv_List == tableView){
            let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "AppointmentListCell" : "AppointmentListCell", for: indexPath as IndexPath) as! AppointmentListCell
            
            let dict = ((aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary).removeNullFromDict()
            cell.lblCName.text = "\(dict.value(forKey: "customer_name")!)" == "" ? "N/A" : "\(dict.value(forKey: "customer_name")!)"
            
            cell.lblMobileNumber.text = "\(dict.value(forKey: "customer_mobile")!)" == "" ? "N/A" : "\(dict.value(forKey: "customer_mobile")!)"
            
            cell.lblAddress.text = "\(dict.value(forKey: "address")!)" == "" ? "N/A" : "\(dict.value(forKey: "address")!)"

            cell.lblAppointmentDate.text = "\(dict.value(forKey: "slot_date")!)" == "" ? "N/A" : "\(dict.value(forKey: "slot_date")!)"

            cell.lblAppointmentTime.text = "\(dict.value(forKey: "time_slot")!)" == "" ? "N/A" : "\(dict.value(forKey: "time_slot")!)"
                
            cell.lblAppointmentAmount.text = "\(dict.value(forKey: "order_total")!)" == "\u{20B9}0.0" ? "N/A" : "\u{20B9}\(dict.value(forKey: "order_total")!)"
            
            cell.lblAssignTo.text = "\(dict.value(forKey: "assigned_to")!)" == "" ? "N/A" : "\(dict.value(forKey: "assigned_to")!)"
                 
            cell.lblCreatedDate.text = "\(dict.value(forKey: "order_date")!)" == "" ? "N/A" : "\(dict.value(forKey: "order_date")!)"
           
            cell.lblAppointmentStatus.text = "\u{2022} \(dict.value(forKey: "status")!)"
            
            cell.lblstaffRating.text = "\(dict.value(forKey: "order_staff_rating")!)" == "" ? "N/A" : "\(dict.value(forKey: "order_staff_rating")!)"
            cell.lblComment.text = "\(dict.value(forKey: "order_staff_comments")!)" == "" ? "N/A" : "\(dict.value(forKey: "order_staff_comments")!)"
            
            
            if("\(dict.value(forKey: "status")!)" == "Completed" ){
                cell.lblAppointmentStatus.textColor = UIColor.blue
            }else if("\(dict.value(forKey: "status")!)" == "Assigned" ){
                cell.lblAppointmentStatus.textColor = UIColor.orange
            }else{
                cell.lblAppointmentStatus.textColor = UIColor.darkGray
            }
            
            if(self.segmentedControl.selectedSegmentIndex == 0){
                cell.btnAssignStaffHeight.constant = 35.0
                cell.btnAssignStaff.tag = indexPath.row
                cell.btnAssignStaff.addTarget(self, action: #selector(unFollowActionAssignStaff), for: .touchUpInside)
            }else{
                cell.btnAssignStaffHeight.constant = 0.0
            }
            
            return cell
        }else{
            let cell = tv_StaffList.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "StaffCell" : "StaffCell", for: indexPath as IndexPath) as! StaffCell
            let dict = aryStaffList.object(at: indexPath.row)as! NSDictionary
            cell.lblName.text = "\(dict.value(forKey: "name")!)"
            cell.btnMobile.setTitle("\(dict.value(forKey: "phone")!)", for: .normal)
            cell.btnMobile.tag = indexPath.row
            cell.btnMobile.addTarget(self, action: #selector(unFollowActionCall), for: .touchUpInside)

            return cell
        }
        
        
    
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(self.tv_List == tableView){
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
             self.view.endEditing(true)
             let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryDetailVC") as? HistoryDetailVC
             vc?.dictDATA = dict.mutableCopy()as! NSMutableDictionary
             self.navigationController?.pushViewController(vc!, animated: true)
        }else{
            let dict = aryStaffList.object(at: indexPath.row)as! NSDictionary
            self.AssignStaffAPI(staffID: "\(dict.value(forKey: "user_id")!)", orderID: self.orderID )
            UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
                self.viewStaff.removeFromSuperview()
            }, completion: nil)
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryList.count == 0 ? 100 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
    @objc func unFollowActionAssignStaff(sender : UIButton) {
        let dict = aryList.object(at: sender.tag)as! NSDictionary
        self.orderID = "\(dict.value(forKey: "order_id")!)"
        self.viewStaff.frame = self.view.frame
        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.view.addSubview(self.viewStaff)
            if(self.aryStaffList.count == 0){
                self.StaffListVCListAPI()
            }
            
        }, completion: nil)
       
    }
    
    @objc func unFollowActionCall(sender : UIButton) {
        print("Button wurde gedrückt")
        let dict = aryStaffList.object(at: sender.tag)as! NSDictionary

        if !(callingFunction(number: "\(dict.value(forKey: "phone")!)" as NSString)){
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: alertCalling, viewcontrol: self)
        }
    }
}
// MARK: -
// MARK: ----------AppintmentCell---------
class AppointmentListCell: UITableViewCell {
    @IBOutlet weak var lblCName: UILabel!
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblAppointmentDate: UILabel!
    @IBOutlet weak var lblAppointmentTime: UILabel!
    @IBOutlet weak var lblAppointmentAmount: UILabel!
    @IBOutlet weak var lblAppointmentStatus: UILabel!
    @IBOutlet weak var lblAssignTo: UILabel!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var btnAssignStaff: UIButton!
    @IBOutlet weak var btnAssignStaffHeight: NSLayoutConstraint!
    @IBOutlet weak var btnExtraService: UIButton!
    @IBOutlet weak var btnCompleteService: UIButton!
    @IBOutlet weak var btnExtraServiceHeight: NSLayoutConstraint!
    @IBOutlet weak var btnCompleteServiceHeight: NSLayoutConstraint!
    @IBOutlet weak var lblstaffRating: UILabel!
    @IBOutlet weak var lblComment: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: -
// MARK: - ---------------API Calling----------

extension AppointmentListVC  {
    func AppointmentListAPI(forLoader : Int) {
        if(isInternetAvailable()){
            var loader = UIAlertController()
            if( forLoader == 1 ){
                 loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            } else{
                self.activityIndicator.isHidden = false
            }
            
            let dictdata = NSMutableDictionary()
            dictdata.setValue("", forKey: "status")
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_get_Allorders) { [self] (responce, status) in
                if( forLoader == 1 ){
                    loader.dismiss(animated: false, completion: nil)
                } else{
                    self.activityIndicator.isHidden = true
                      self.refreshControl.endRefreshing()
                }
             
                   print(responce)
                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    self.aryList = NSMutableArray()
                    if(dict.value(forKey: "orders") != nil){
                        self.filterData(ary: (dict.value(forKey: "orders")as! NSArray).mutableCopy() as! NSMutableArray)
                    }else{
                        self.reloadTable()
                    }
                
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func AssignStaffAPI(staffID : String , orderID : String) {
        if(isInternetAvailable()){
          
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)

            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(staffID)", forKey: "user_id")
            dictdata.setValue("\(orderID)", forKey: "order_id")

            WebService.callAPIBYPOST(parameter: dictdata, url: URL_assign_booking) { [self] (responce, status) in
           
                loader.dismiss(animated: false, completion: nil)
                   print(responce)
                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    let alert = UIAlertController(title: "Alert", message: dict.value(forKey: "message") as? String, preferredStyle: .alert)
                    alert.view.tintColor = UIColor.black
                    alert.addAction(UIAlertAction(title: "Go to Assigned List", style: .default, handler: { action in
                        self.segmentedControl.selectedSegmentIndex = 1
                        self.aryList = NSMutableArray()
                        self.tv_List.reloadData()
                        self.AppointmentListAPI(forLoader: 1)
                        
                    }))
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.aryList = NSMutableArray()
                        self.tv_List.reloadData()
                        self.AppointmentListAPI(forLoader: 1)
                        
                    }))
              
                    self.present(alert, animated: true, completion: nil)
                   
                
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func StaffListVCListAPI() {
        if(isInternetAvailable()){
            self.activityIndicatorStaff.isHidden = false
            let dictdata = NSMutableDictionary()
            WebService.callAPIBYGET(parameter: dictdata, url: URL_get_all_staff) { (responce, status) in
              self.activityIndicatorStaff.isHidden = true

                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    self.aryStaffList = NSMutableArray()
                    if(dict.value(forKey: "staffs") != nil){
                        self.aryStaffList = (dict.value(forKey: "staffs")as! NSArray).mutableCopy() as! NSMutableArray
                      
                    }
                    self.tv_StaffList.reloadData()
                    self.tv_StaffList.delegate = self
                    self.tv_StaffList.dataSource = self
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func filterData(ary : NSMutableArray) {
        self.aryProcessing = NSMutableArray()
        self.aryAssigned = NSMutableArray()
        self.aryCompleted = NSMutableArray()

        for item in ary {
            let dict = (item as AnyObject) as! NSDictionary
            let status = "\(dict.value(forKey: "status")!)".lowercased()
            if(status == "processing"){
                self.aryProcessing.add(dict)
            }
            else if(status == "assigned"){
                self.aryAssigned.add(dict)
            }else if(status == "completed"){
                self.aryCompleted.add(dict)
            }
        }
        reloadTable()
    }
    
}

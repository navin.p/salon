//
//  MyProfileVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/6/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class MyProfileVC: UIViewController {
    //MARK: --------------SIGNUP--------------
    
  
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtSignUserName: ACFloatingTextfield!
    var imagePicker = UIImagePickerController()
    var strIMagename = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(globleLogInData)
        txtSignUserName.text = "\(globleLogInData.value(forKey: "full_name")!)"
        txtEmail.text = "\(globleLogInData.value(forKey: "user_email")!)"
        txtMobile.text = "\(globleLogInData.value(forKey: "user_phone")!)"

        let urlImage = "\(globleLogInData.value(forKey: "profile_pic")!)"
        imgProfile.contentMode = .center
        imgProfile.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "dummy_user"), options: SDWebImageOptions(rawValue: 0), progress: { (S1, S2) in
        }, completed: { [self] (IMAGE, ERROR, cacheType, imageURL) in
            if(IMAGE != nil){
                imgProfile.contentMode = .scaleAspectFill
            }else{
                imgProfile.contentMode = .center
            }
        }, usingActivityIndicatorStyle: .gray)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imgProfile.layer.cornerRadius = 42.0
        imgProfile.layer.borderWidth = 1.0
        imgProfile.layer.borderColor = UIColor.lightGray.cgColor
    }
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnMenu(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        
        let nextVc: DrawerVC = mainStoryboard.instantiateViewController(withIdentifier: "DrawerVC") as! DrawerVC
        nextVc.handleDrawerView = self
        nextVc.modalPresentationStyle = .overCurrentContext
        nextVc.modalTransitionStyle = .crossDissolve
        self.present(nextVc, animated: false, completion: nil)
        
    }
    @IBAction func actionOnEdit(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        let camera = (UIAlertAction(title: alert_CAMERA, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertCalling, viewcontrol: self)

                
            }
        }))
        camera.setValue(#imageLiteral(resourceName: "002-camera"), forKey: "image")
        camera.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(camera)
        
        let Gallery = (UIAlertAction(title: alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertCalling, viewcontrol: self)

                
            }
            
        }))
        
        Gallery.setValue(#imageLiteral(resourceName: "001-gallery"), forKey: "image")
        Gallery.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Gallery)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Add Cancel  button")
        }))
        
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    @IBAction func actionOnUpdate(_ sender: UIButton) {
        if(strIMagename != ""){
            UpdateProfileAPI()
        }
    }
    @IBAction func actionOnBottomButton(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.view.endEditing(true)
        var count = 0
        if(sender.tag == 0){ // Service
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DashBoardVC.self) {
                    count = 1
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            if(count == 0){
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
        else if(sender.tag == 1){ //Offer
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: Offervc.self) {
                    count = 1
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            if(count == 0){
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "Offervc") as? Offervc
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
        else if(sender.tag == 2){ //Appoinment
            if(globleLogInData.count == 0){
                let alert = UIAlertController(title: alertMsg, message: alertLogin, preferredStyle: UIAlertController.Style.alert)
                alert.view.tintColor = UIColor.black
                alert.addAction(UIAlertAction (title: "LOGIN", style: .default, handler: { (nil) in
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                    self.navigationController?.pushViewController(vc!, animated: true)
                }))
                alert.addAction(UIAlertAction (title: "CLOSE", style: .default, handler: { (nil) in
                    
                   // self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
            
            
            
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: AppoinmentVC.self) {
                    count = 1
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            if(count == 0){
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AppoinmentVC") as? AppoinmentVC
                self.navigationController?.pushViewController(vc!, animated: false)
            }
                
            }
        }
        else if(sender.tag == 3){ //Profile
           
        }
        
    }

}
// MARK: - ------------DrawerScreenDelegate---------
// MARK: -
extension MyProfileVC: DrawerScreenDelegate {
    func refreshDrawerScreen(strType: String, tag: Int) {
            if(strType == "Sign out"){
                       self.view.endEditing(true)
                       let alert = UIAlertController(title: "Sign out", message: alertLogout, preferredStyle: .alert)
                       alert.view.tintColor = UIColor.black
                       let Yes = (UIAlertAction(title: "Yes", style: .default , handler:{ (UIAlertAction)in
                           self.LogOutAPI()
                       }))
                       alert.addAction(Yes)
                   
                       alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                           print("User click Add Cancel  button")
                       }))
                       
                       
                       if let popoverController = alert.popoverPresentationController {
                           popoverController.sourceView = self.view
                           popoverController.sourceRect = self.view.bounds
                           popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                       }
                       
                       self.present(alert, animated: true, completion: {
                           print("completion block")
                       })
                       
                   }
                   
                   
                   else if (strType == "Change Password"){
                       self.view.endEditing(true)
                           let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC
                           self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Feedback"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "FeedBackVC") as? FeedBackVC
                self.navigationController?.pushViewController(vc!, animated: true)
        }
        else if (strType == "About Us"){
                      self.view.endEditing(true)
                      let vc = mainStoryboard.instantiateViewController(withIdentifier: "AboutUSVC") as? AboutUSVC
                      self.navigationController?.pushViewController(vc!, animated: true)
              }
        else if (strType == "Contact Us"){
                      self.view.endEditing(true)
                      let vc = mainStoryboard.instantiateViewController(withIdentifier: "ContactUSVC") as? ContactUSVC
                      self.navigationController?.pushViewController(vc!, animated: true)
              }
        else if (strType == "Assigned Booking List"){
                            self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AssignedBookingListVC") as? AssignedBookingListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Glimpses of Gallery"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "GlimpsesofGalleryVC") as? GlimpsesofGalleryVC
                self.navigationController?.pushViewController(vc!, animated: true)
        }
            else if (strType == "History"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryVC") as? HistoryVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }  else if (strType == "Review"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ReviewListVC") as? ReviewListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }  else if (strType == "Appoinment List"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentListVC") as? AppointmentListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Staff List"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "StaffListVC") as? StaffListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Profile"){ //Profile
                var count = 0
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: MyProfileVC.self) {
                        count = 1
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                if(count == 0){
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
            }
            else if (strType == "Home"){
                self.view.endEditing(true)
                var count = 0
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: DashBoardVC.self) {
                        count = 1
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                if(count == 0){
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
         
            }
            else if (strType == "Login"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                vc!.view.tag = 1

                self.navigationController?.pushViewController(vc!, animated: false)
         
            }
            else if (strType == "Signup"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                vc!.view.tag = 2

                self.navigationController?.pushViewController(vc!, animated: false)
         
            }
    }
    
    
    
}
// MARK: -
// MARK: ---------UIImagePickerControllerDelegate------
extension MyProfileVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        self.imgProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.imgProfile.contentMode = .scaleAspectFill
        strIMagename = getUniqueString() + "iOSProfile"
    }
}
// MARK: -
// MARK: - ---------------API Calling----------

extension MyProfileVC  {
    func UpdateProfileAPI() {
        if(isInternetAvailable()){
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue(strIMagename, forKey: "photo")
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            WebService.callAPIWithImage(parameter: dictdata, url: URL_update_profile_pic, image: self.imgProfile.image!, fileName: "photo", withName: strIMagename) { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                if (status){
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    print(dict)
                    if("\(dict.value(forKey: "error")!)" == "1"){
                        showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        self.LoginAPI()
                        showToast(message: "\(dict.value(forKey: "message")!)", font: UIFont.systemFont(ofSize: 10) , viewww: self.view)
                        self.view.endEditing(true)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)
                }
            }
   
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)

        }
    }
    func LoginAPI() {
        if(isInternetAvailable()){
            let dictdata = NSMutableDictionary()
            print(globleLogInData)
            dictdata.setValue("\(globleLogInData.value(forKey: "user_phone")!)", forKey: "u_phone")
            //dictdata.setValue("\(globleLogInData.value(forKey: "password")!)", forKey: "u_pass")
            dictdata.setValue(str_DeviceID, forKey: "device_id")
            dictdata.setValue(fcm_token, forKey: "device_token")
            dictdata.setValue(platform, forKey: "platform")
            
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_Login) { (responce, status) in
                        if (status){
                            let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            print(dict)
                            if("\(dict.value(forKey: "error")!)" == "1"){
                                let alert = UIAlertController(title: "Failed", message: "\(dict.value(forKey: "message")!)", preferredStyle: UIAlertController.Style.alert)
                                alert.view.tintColor = UIColor.black
                                // add the actions (buttons)
                                alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                                    UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                                    UserDefaults.standard.synchronize()
                                    
                                    self.view.endEditing(true)
                                    let redViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as! LoginSigUpVC
                                    let navigationController = UINavigationController(rootViewController: redViewController)

                                    UIApplication.shared.windows.first?.rootViewController = navigationController
                                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                                    navigationController.setNavigationBarHidden(true, animated: true)
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                          
                            }else{
                                let loginData = (dict.value(forKey: "response")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                
                                loginData.setValue(globleLogInData.value(forKey: "Remember"), forKey: "Remember")
                                loginData.setValue("\(dict.value(forKey: "role")!)", forKey: "role")
                                loginData.setValue("\(globleLogInData.value(forKey: "password")!)", forKey: "password")
                                    nsud.setValue(loginData, forKey: "Salon_LoginData")
                                    nsud.synchronize()
                                globleLogInData = NSMutableDictionary()
                                globleLogInData = (nsud.value(forKey: "Salon_LoginData")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            }
                        }else{
                            showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)
                        }
               }
        }
        
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)

        }
    }
    func LogOutAPI() {
         if(isInternetAvailable()){
             let dictdata = NSMutableDictionary()
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)

            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
             WebService.callAPIBYPOST(parameter: dictdata, url: URL_LogOut) { (responce, status) in
                 if (status){
                    loader.dismiss(animated: false, completion: nil)
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary

                    
                    if("\(dict.value(forKey: "error")!)" == "1"){
                        showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        UserDefaults.standard.synchronize()
                        globleLogInData = NSMutableDictionary()

                        self.view.endEditing(true)
                        let redViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as! LoginSigUpVC
                        let navigationController = UINavigationController(rootViewController: redViewController)

                        UIApplication.shared.windows.first?.rootViewController = navigationController
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
                        navigationController.setNavigationBarHidden(true, animated: true)
                    }
                    
                 }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                 }
             }
         }
         else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
         }
     }
}

//
//  AboutUSVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/6/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit
import WebKit

class AboutUSVC: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let myURL = URL(string:URL_aboutus)
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
        webView.navigationDelegate = self
    }
 
    // MARK: - ------IBAction----------
       // MARK: -
       @IBAction func actionBack(_ sender: UIButton) {
           scaleAnimationONUIButton(sender: sender)
           self.navigationController?.popViewController(animated: true)
       }
       

}
extension AboutUSVC : UIWebViewDelegate,WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        indicator.isHidden = false
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        indicator.isHidden = true

    }
}

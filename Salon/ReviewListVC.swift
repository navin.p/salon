//
//  ReviewListVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/8/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class ReviewListVC: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lbltitle: UILabel!
    var aryList = NSMutableArray()
    var refreshControl = UIRefreshControl()
    // MARK: - ------LifeCycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        tv_List.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
         refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
         tv_List.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true
        if(aryList.count == 0){
             self.ReviewListListAPI()
        }else{
            self.tv_List.reloadData()
            self.tv_List.delegate = self
            self.tv_List.dataSource = self
        }
    }
    @objc func refresh(_ sender: AnyObject) {
        self.ReviewListListAPI()
    }
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension ReviewListVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "ReviewListCell" : "ReviewListCell", for: indexPath as IndexPath) as! ReviewListCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        cell.lblName.text = "\(dict.value(forKey: "comments")!)"
        cell.lbldate.text = "\(dict.value(forKey: "date")!)"
        cell.viewRating.contentMode = UIView.ContentMode.scaleAspectFit
        cell.viewRating.type = .halfRatings
        let rating = "\(dict.value(forKey: "rating")!)" == "" ? "0" : "\(dict.value(forKey: "rating")!)"
        cell.viewRating.rating = Double(rating)!
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryList.count == 0 ? 100 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
    

}
// MARK: -
// MARK: ----------ReviewListCell---------
class ReviewListCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var viewRating: FloatRatingView!

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: -
// MARK: - ---------------API Calling----------

extension ReviewListVC  {
    func ReviewListListAPI() {
        if(isInternetAvailable()){
         self.activityIndicator.isHidden = false
            let dictdata = NSMutableDictionary()
            WebService.callAPIBYGET(parameter: dictdata, url: URL_get_feedbacks) { (responce, status) in
              self.activityIndicator.isHidden = true
                self.refreshControl.endRefreshing()

                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    self.aryList = NSMutableArray()
                    if(dict.value(forKey: "feedbacks") != nil){
                        self.aryList = (dict.value(forKey: "feedbacks")as! NSArray).mutableCopy() as! NSMutableArray
                      
                    }
                    self.tv_List.reloadData()
                    self.tv_List.delegate = self
                    self.tv_List.dataSource = self
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
}

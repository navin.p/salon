//
//  PromoCodeVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/8/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

//MARK: -------Protocol

protocol PromocodeDelegate : class{
    func getDataFromPromocodeDelegate(dictData : NSDictionary ,tag : Int)
    
}
class PromoCodeVC: UIViewController {
    
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lbltitle: UILabel!
    var aryList = NSMutableArray()
    var refreshControl = UIRefreshControl()
    open weak var delegate:PromocodeDelegate?

    // MARK: - ------LifeCycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        tv_List.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
         refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
         tv_List.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true
        if(aryList.count == 0){
             self.PromocodeListAPI()
        }else{
            self.tv_List.reloadData()
            self.tv_List.delegate = self
            self.tv_List.dataSource = self
        }
    }
    @objc func refresh(_ sender: AnyObject) {
        self.PromocodeListAPI()
    }
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.dismiss(animated: true, completion: nil)
    }


}

// MARK: - ----------------UITableViewDelegate
// MARK: -

extension PromoCodeVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "PromoCell" : "PromoCell", for: indexPath as IndexPath) as! PromoCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        cell.lblName.text = "\(dict.value(forKey: "coupon_name")!)"
        cell.lblDetail.text = "\(dict.value(forKey: "coupon_desc")!)"
        
        let type = "\(dict.value(forKey: "discount_type")!)"
        if (type == "percent") {
            cell.lblDiscount.text = "\(dict.value(forKey: "discount_amount")!)%"
        }else{
            cell.lblDiscount.text = "\u{20B9}\(dict.value(forKey: "discount_amount")!)"
        }
        cell.btnApply.tag = indexPath.row
        cell.btnApply.addTarget(self, action: #selector(unFollowAction), for: .touchUpInside)

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        ApplyPromoCodeAPI(dict: dict)
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryList.count == 0 ? 100 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
    
    @objc func unFollowAction(sender : UIButton) {
        let dict = aryList.object(at: sender.tag)as! NSDictionary
        self.ApplyPromoCodeAPI(dict: dict)
    }
}
// MARK: -
// MARK: ----------OfferCell---------
class PromoCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var btnApply: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: -
// MARK: - ---------------API Calling----------

extension PromoCodeVC  {
    func PromocodeListAPI() {
        if(isInternetAvailable()){
         self.activityIndicator.isHidden = false
            let dictdata = NSMutableDictionary()
            WebService.callAPIBYGET(parameter: dictdata, url: URL_get_promocodes) { (responce, status) in
              self.activityIndicator.isHidden = true
                self.refreshControl.endRefreshing()

                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    self.aryList = NSMutableArray()
                    
                    if("\(dict.value(forKey: "error")!)" == "1"){
                       showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        if(dict.value(forKey: "coupons") != nil){
                            self.aryList = (dict.value(forKey: "coupons")as! NSArray).mutableCopy() as! NSMutableArray
                          
                        }
                    }
                    
                    
                
                    self.tv_List.reloadData()
                    self.tv_List.delegate = self
                    self.tv_List.dataSource = self
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func ApplyPromoCodeAPI(dict: NSDictionary) {
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            let dictdata = NSMutableDictionary()
        
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue("\(dict.value(forKey: "coupon_id")!)", forKey: "promo_id")

            WebService.callAPIBYPOST(parameter: dictdata, url: URL_apply_promocode) { [self] (responce, status) in
                loader.dismiss(animated: false, completion: nil)

                if (status){
                    let dict1 = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    
                    if("\(dict1.value(forKey: "status")!)" != "success"){
                       showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict1.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        dict1.setValue(dict, forKey: "promocode")
                        self.dismiss(animated: true, completion: nil)
                        self.delegate?.getDataFromPromocodeDelegate(dictData: dict1, tag: 0)

                    }
                   
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
}

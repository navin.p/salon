//
//  ChangePasswordVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/6/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    // MARK: - -----IBOutlet----------
    // MARK: -
    @IBOutlet weak var txtOLDPassword: ACFloatingTextfield!
    @IBOutlet weak var txtNewPassword: ACFloatingTextfield!
    @IBOutlet weak var txtCnewPassword: ACFloatingTextfield!
    
    // MARK: - ------Life Cycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        if(validationForChangePAss()){
            ChangePasswordAPI()
        }
    }

}
// MARK:
// MARK:- Validation

extension ChangePasswordVC {
    func validationForChangePAss() -> Bool {
        
        if(txtOLDPassword.text?.count == 0){
            addErrorMessage(textView: txtOLDPassword, message: alert_required)
            return false
        }
        
        else if("\(globleLogInData.value(forKey: "password")!)" != txtOLDPassword.text){
            addErrorMessage(textView: txtOLDPassword, message: alert_OldInvalidPassword)
            return false
        }
        
        else if(txtNewPassword.text?.count == 0){
            addErrorMessage(textView: txtNewPassword, message: alert_required)
            return false
        }
        else if(txtCnewPassword.text?.count == 0){
            addErrorMessage(textView: txtCnewPassword, message: alert_required)
            return false
        }
        else if(txtNewPassword.text != txtCnewPassword.text){
            addErrorMessage(textView: txtCnewPassword, message: alert_Password_CPassword)
            return false
        }
        return true
    }
   
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- -------------UITextFieldDelegate----------

extension ChangePasswordVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

            return TextValidation(textField: textField, string: string, returnOnly: "", limitValue: 45)
    }
}

// MARK: -
// MARK: - API Calling

extension ChangePasswordVC  {
    func ChangePasswordAPI() {
        if(isInternetAvailable()){
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue("\(txtNewPassword.text!)", forKey: "password")

            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_change_password) { [self] (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                        if (status){
                            let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            
                            print(dict)
                            if("\(dict.value(forKey: "error")!)" == "1"){
                               showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                            }else{
                                globleLogInData.setValue("\(self.txtNewPassword.text!)", forKey: "password")
                                showToast(message: "\(dict.value(forKey: "message")!)", font: UIFont.systemFont(ofSize: 12.0) , viewww: self.view)

                                self.navigationController?.popViewController(animated: false)
                            }
                     
                        }else{
                            showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)


                        }
                    }
            //
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
}

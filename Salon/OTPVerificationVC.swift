//
//  OTPVerificationVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/5/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class OTPVerificationVC: UIViewController {

    
    var dictDAta = NSMutableDictionary()
    var strComeFrom = ""
    var timer : Timer!
    var second = 30
    //MARK: --------------IBOutlet-----------
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var txtOTP: ACFloatingTextfield!
    @IBOutlet weak var lblresend: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    //MARK: --------------LifeCycle------------
    override func viewDidLoad() {
        super.viewDidLoad()
        timer = Timer()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.calculateSeconds), userInfo: nil, repeats: true)
        lbltitle.text = "A One time passcode has been sent to \(dictDAta.value(forKey: "user_phone")!).\n\nPlease enter the OTP below to verify your account."

    }
    
    //MARK: --------------IBAction------------


    @IBAction func actionSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        scaleAnimationONUIButton(sender: sender)
        if(validationForOTP()){
            self.OTPVerificationAPI()
        }
    }
    
    @IBAction func actionResend(_ sender: UIButton) {
        self.view.endEditing(true)
        txtOTP.text = ""
        self.OTPResendAPI()
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.navigationController?.popViewController(animated: true)
    }
    @objc func calculateSeconds() {
         second -= 1
         whatever()
    }
    func whatever() {
         if second < 30 && second > 0 {
            lblCount.text = "00:\(second)"
            lblresend.isHidden = true
         }
         else {
            lblCount.text = ""
            lblresend.isHidden = false
            second = 30
            timer.invalidate()
             timer = nil
        }
        
    }
    
}
// MARK:
// MARK:- Validation

extension OTPVerificationVC {
    func validationForOTP() -> Bool {
        
        if(txtOTP.text?.count == 0){
            addErrorMessage(textView: txtOTP, message: alert_MobileNumber)
            return false
        }
        return true
    }
   
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK: -
// MARK: - API Calling

extension OTPVerificationVC  {
    func OTPVerificationAPI() {
        if(isInternetAvailable()){
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(txtOTP.text!)", forKey: "otp")
            dictdata.setValue("\(dictDAta.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue(strComeFrom == "Login" ? "account_verify" : "forgot_password", forKey: "type")

        
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_OtpVerify) { [self] (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                        if (status){
                            let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            
                            print(dict)
                            if("\(dict.value(forKey: "error")!)" == "1"){
                               showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                            }else{

                                if(strComeFrom == "Login"){
                                    self.LoginAPI()
                                }
                                let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                                self.navigationController?.pushViewController(vc!, animated: true)
                            }
                     
                        }else{
                            showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)


                        }
                    }
            //
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func OTPResendAPI() {
        if(isInternetAvailable()){
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(dictDAta.value(forKey: "user_id")!)", forKey: "user_id")
            
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_OtpResend) { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                        if (status){
                            let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            
                            print(dict)
                            if("\(dict.value(forKey: "error")!)" == "1"){
                               showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                            }else{
                                showAlertWithoutAnyAction(strtitle: "Success", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)

                                self.timer = Timer()
                                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.calculateSeconds), userInfo: nil, repeats: true)
                            }
                     
                        }else{
                            showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)


                        }
                    }
            //
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func LoginAPI() {
        if(isInternetAvailable()){
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(dictDAta.value(forKey: "user_phone")!)", forKey: "u_phone")
            dictdata.setValue("\(dictDAta.value(forKey: "password")!)", forKey: "u_pass")
            dictdata.setValue(str_DeviceID, forKey: "device_id")
            dictdata.setValue(fcm_token, forKey: "device_token")
            dictdata.setValue(platform, forKey: "platform")
            
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_Login) { [self] (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                        if (status){
                            let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            print(dict)
                            if("\(dict.value(forKey: "error")!)" == "1"){
                               showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                            }else{
                                let loginData = (dict.value(forKey: "response")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                                loginData.setValue("\(self.dictDAta.value(forKey: "Remember")!)", forKey: "Remember")
                                loginData.setValue("\(self.dictDAta.value(forKey: "role")!)", forKey: "role")
                                nsud.setValue(loginData, forKey: "Salon_LoginData")
                                nsud.synchronize()
                                self.view.endEditing(true)
                                let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                                self.navigationController?.pushViewController(vc!, animated: true)
                                
               
                            }
                     
                        }else{
                            showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)


                        }
                    }
            //
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)

        }
    }
}

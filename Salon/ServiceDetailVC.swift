//
//  ServiceDetailVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/7/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class ServiceDetailVC: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var lbltitleProduct: UILabel!
    @IBOutlet weak var lbltitleDetail: UILabel!
    @IBOutlet weak var lbltitleRealAmount: UILabel!
    @IBOutlet weak var lbltitleAmount: UILabel!
    @IBOutlet weak var btnAddCart: UIButton!

    var dictData = NSMutableDictionary()
    var dictDetailList = NSMutableDictionary()

    // MARK: - ------LifeCycle----------
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbltitle.text = "\(dictData.value(forKey: "title")!)"
        btnAddCart.isHidden = true
      
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true
             self.ServiceDetailAPI()
    }
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func actionOnAddtocart(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)

        ServiceAddTocartAPI()
        
    }

  

}
// MARK: -
// MARK: - ---------------API Calling----------

extension ServiceDetailVC  {
    func ServiceDetailAPI() {
        if(isInternetAvailable()){
         self.activityIndicator.isHidden = false
            let dictdata = NSMutableDictionary()

            dictdata.setValue("\(dictData.value(forKey: "id")!)", forKey: "product_id")
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_get_product_detail) { (responce, status) in
              self.activityIndicator.isHidden = true
 print(responce)
                if (status){
                    self.dictDetailList = NSMutableDictionary()
                    self.dictDetailList = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    self.setDataOnView(dict:  self.dictDetailList)
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func ServiceAddTocartAPI() {
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)

            let dictdata = NSMutableDictionary()

            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue("\(dictData.value(forKey: "id")!)", forKey: "pid")
            dictdata.setValue("0", forKey: "variation_id")
            dictdata.setValue("1", forKey: "quantity")
        
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_add_cart) { (responce, status) in
                print(responce)
                loader.dismiss(animated: false, completion: nil)

                if (status){
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    if("\(dict.value(forKey: "error")!)" == "1"){
                       showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        let alert = UIAlertController(title: alertInfo, message: "\(dict.value(forKey: "message")!)", preferredStyle: UIAlertController.Style.alert)
                        alert.view.tintColor = UIColor.black
                        // add the actions (buttons)appointment
                        alert.addAction(UIAlertAction (title: "Go to Appointment", style: .default, handler: { (nil) in
                            let vc = mainStoryboard.instantiateViewController(withIdentifier: "AppoinmentVC") as? AppoinmentVC
                            self.navigationController?.pushViewController(vc!, animated: true)
                        }))
                        alert.addAction(UIAlertAction (title: "OK", style: .default, handler: { (nil) in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)
                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func setDataOnView(dict : NSMutableDictionary)  {
        self.lbltitleProduct.text = "\(dict.value(forKey: "title")!)"
        self.lbltitleDetail.attributedText = "\(dict.value(forKey: "description")!)".htmlToAttributedString
        self.lbltitleAmount.text = "\u{20B9}\(dict.value(forKey: "price")!)"
        self.lbltitleRealAmount.text = "\u{20B9}\(dict.value(forKey: "regular_price")!)"
  
        if("\(dict.value(forKey: "regular_price")!)" == ""){
            self.lbltitleRealAmount.text = ""
        }
        if("\(dict.value(forKey: "regular_price")!)" == "\(dict.value(forKey: "price")!)"){
            self.lbltitleRealAmount.text = ""
        }
        
 
        if(globleLogInData.count != 0){
            let role = "\(globleLogInData.value(forKey: "role")!)"
            if(role == "customer"){
                btnAddCart.isHidden = false
            }
        }
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

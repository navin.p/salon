//
//  ViewBillVC.swift
//  GNGPL
//
//  Created by Navin Patidar on 12/28/18.
//  Copyright © 2018 Saavan_patidar. All rights reserved.
//

import UIKit

class DrawerVC: UIViewController {
    @IBOutlet weak var tvlist: UITableView!
    
    var aryMenuItems = NSMutableArray()
    weak var handleDrawerView: DrawerScreenDelegate?
    
    // MARK: - ---------------LifeCycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        //lblVersion.text = "Version - " + app_Version
        tvlist.tableFooterView = UIView()
        tvlist.estimatedRowHeight = 80.0
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.view.backgroundColor = UIColor.clear
        aryMenuItems = NSMutableArray()
        
        if(globleLogInData.count == 0){
            aryMenuItems = [["title":"Home","image":"home"],["title":"About Us","image":"my_qr_code"],["title":"Contact Us","image":"my_qr_code"],["title":"Glimpses of Gallery","image":"find_tree"],["title":"Review","image":"about_tree-1"]]
        }else{
            let role = "\(globleLogInData.value(forKey: "role")!)"
            if(role == "customer"){
                aryMenuItems = [["title":"Home","image":"home"],["title":"Change Password","image":"associated_partner"],["title":"History","image":"tree_cutting"],["title":"About Us","image":"my_qr_code"],["title":"Contact Us","image":"my_qr_code"],["title":"Glimpses of Gallery","image":"find_tree"],["title":"Feedback","image":"near_by_tree"],["title":"Review","image":"about_tree-1"],["title":"Sign out","image":"did_you_know"]]
          
            }else if(role == "staff"){
                aryMenuItems = [["title":"Home","image":"home"],["title":"Change Password","image":"associated_partner"],["title":"Assigned Booking List","image":"tree_cutting"],["title":"About Us","image":"my_qr_code"],["title":"Contact Us","image":"my_qr_code"],["title":"Glimpses of Gallery","image":"find_tree"],["title":"Feedback","image":"near_by_tree"],["title":"Review","image":"about_tree-1"],["title":"Sign out","image":"did_you_know"]]

            }
            else if(role == "administrator"){
                aryMenuItems = [["title":"Home","image":"home"],["title":"Change Password","image":"associated_partner"],["title":"Appointment List","image":"tree_cutting"],["title":"Staff List","image":"my_qr_code"],["title":"About Us","image":"my_qr_code"],["title":"Contact Us","image":"my_qr_code"],["title":"Glimpses of Gallery","image":"find_tree"],["title":"Feedback","image":"near_by_tree"],["title":"Review","image":"about_tree-1"],["title":"Sign out","image":"did_you_know"]]
            }
        }
        
     
        
        tvlist.delegate = self
        tvlist.dataSource = self
        tvlist.tableFooterView = UIView()
        tvlist.reloadData()
        //self.view.backgroundColor = UIColor.white
    }
    
    // MARK: - ----------------IBAction
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.dismiss(animated: false) {}
    }
    
}


// MARK: - ----------------UITableViewDelegate
// MARK: -

extension DrawerVC : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return aryMenuItems.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section != 0 {
            let cell = tvlist.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath as IndexPath) as! DrawerCell
            let strTitle = (aryMenuItems[indexPath.row]as AnyObject).value(forKey: "title")as! String
            cell.btnTitle.setTitle(strTitle, for: .normal)
            cell.backgroundColor = hexStringToUIColor(hex: primaryColor)
            return cell
        }else{
            if(globleLogInData.count == 0){
                let cell = tvlist.dequeueReusableCell(withIdentifier: "Drawer3", for: indexPath as IndexPath) as! DrawerCell
                cell.backgroundColor = hexStringToUIColor(hex: primaryColor)
                cell.btnLogin.tag = indexPath.row
                cell.btnLogin.addTarget(self, action: #selector(ActionLogin), for: .touchUpInside)
                cell.btnCreatAccount.tag = indexPath.row
                cell.btnCreatAccount.addTarget(self, action: #selector(ActionCreatAccount), for: .touchUpInside)
                return cell
          
            }else{
                let cell = tvlist.dequeueReusableCell(withIdentifier: "Drawer1", for: indexPath as IndexPath) as! DrawerCell
                cell.backgroundColor = hexStringToUIColor(hex: primaryColor)
          
             
                cell.lblName.text = ""
                cell.lblSubtitle.text = ""
                cell.imgProile.contentMode = .center
                cell.imgProile.image = UIImage(named: "user_shape")
          
                cell.lblName.text = "\(globleLogInData.value(forKey: "full_name")!)"
                cell.lblSubtitle.text = "\(globleLogInData.value(forKey: "role")!)"
                let urlImage = "\(globleLogInData.value(forKey: "profile_pic")!)"
                cell.imgProile.contentMode = .center
                cell.imgProile.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "user_shape"), options: SDWebImageOptions(rawValue: 0), progress: { (S1, S2) in
                }, completed: { (IMAGE, ERROR, cacheType, imageURL) in
                    if(IMAGE != nil){
                        cell.imgProile.contentMode = .scaleAspectFill
                    }else{
                        cell.imgProile.contentMode = .center
                    }
                }, usingActivityIndicatorStyle: .gray)
         
       
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                cell.imgProile.layer.cornerRadius =  cell.imgProile.frame.width / 2
                cell.imgProile.layer.borderColor = UIColor.white.cgColor
                cell.imgProile.layer.borderWidth  = 1.0
                cell.imgProile.layer.masksToBounds = false
                cell.imgProile.clipsToBounds = true
                return cell
            }
                
              
            }
      
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if DeviceType.IS_IPAD {
            if indexPath.section == 0 {
                return 132
            }else{
                return 80
            }
        }else{
            if indexPath.section == 0 {
                return 130
            }else{
                return 50
            }
        }
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if(indexPath.section == 0){
            if(globleLogInData.count != 0){
                self.dismiss(animated: false) {
                    self.handleDrawerView?.refreshDrawerScreen(strType: "Profile", tag: indexPath.row)
                }
            }
          
        }else{
            self.dismiss(animated: false) {
                self.handleDrawerView?.refreshDrawerScreen(strType: (self.aryMenuItems[indexPath.row]as AnyObject).value(forKey: "title") as! String, tag: indexPath.row)
            }
        }
    }
    @objc func ActionLogin(sender : UIButton) {
        self.dismiss(animated: false) {
            self.handleDrawerView?.refreshDrawerScreen(strType: "Login", tag: sender.tag)
        }
    }
    @objc func ActionCreatAccount(sender : UIButton) {
        self.dismiss(animated: false) {
            self.handleDrawerView?.refreshDrawerScreen(strType: "Signup", tag: sender.tag)
        }
    }
}
// MARK: - ----------------UserDashBoardCell
// MARK: -
class DrawerCell: UITableViewCell {
    //DashBoard
    
    @IBOutlet weak var imgProile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var btnimg: UIButton!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnCreatAccount: UIButton!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
        self.tintColor = UIColor.white

    }
}
//MARK:
//MARK: ---------------Protocol-----------------
protocol DrawerScreenDelegate : class{
    func refreshDrawerScreen(strType : String ,tag : Int)
}

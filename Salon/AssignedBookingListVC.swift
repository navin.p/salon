//
//  AssignedBookingListVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/6/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class AssignedBookingListVC: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var viewExtraService: UIView!
    @IBOutlet weak var txtAmountExtra: UITextField!
    @IBOutlet weak var txtServiceName: UITextView!
var indexSelection = Int()
    var refreshControl = UIRefreshControl()
    var aryList = NSMutableArray(), aryAssigned = NSMutableArray() , aryCompleted = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        tv_List.tableFooterView = UIView()

        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tv_List.addSubview(refreshControl)
        segmentedControl.addTarget(self, action: #selector(self.indexChanged(_:)), for: .valueChanged)
        self.txtServiceName.layer.cornerRadius = 8.0
        self.txtServiceName.layer.borderWidth = 1.0
        self.txtServiceName.layer.borderColor = UIColor.groupTableViewBackground.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true

        if(aryList.count == 0){
            self.AssignedListAPI(forLoader: 0)
        }else{
           
                self.tv_List.reloadData()
                self.tv_List.delegate = self
                self.tv_List.dataSource = self
        }
    }
    @objc func refresh(_ sender: AnyObject) {
        self.AssignedListAPI(forLoader: 0)
    }
    @objc func indexChanged(_ sender: UISegmentedControl) {
        reloadTable()
    }
    func reloadTable() {
       if segmentedControl.selectedSegmentIndex == 0 {
            print("Select 0")
            self.aryList = NSMutableArray()
            self.aryList = self.aryAssigned
            self.tv_List.reloadData()
        } else if segmentedControl.selectedSegmentIndex == 1 {
            print("Select 1")
            self.aryList = NSMutableArray()
            self.aryList = self.aryCompleted
            self.tv_List.reloadData()
        }
      

        self.tv_List.delegate = self
        self.tv_List.dataSource = self
        self.tv_List.reloadData()
        
    }
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionClose(_ sender: UIButton) {
        self.viewExtraService.removeFromSuperview()
    }
    @IBAction func actionBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSubmit(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        if(txtServiceName.text == ""){
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: "Service Name Required!", viewcontrol: self)

        }else if txtAmountExtra.text == ""{
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: "Extra Service Amount Required!", viewcontrol: self)
        }else{
            self.AddExtraServiceAPI(dict: aryList.object(at: indexSelection)as! NSDictionary)
        }
    }
    

}
// MARK: - ------UITableViewDelegate----------
// MARK: -
extension AssignedBookingListVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.aryList.count
       
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "AppointmentListCell" : "AppointmentListCell", for: indexPath as IndexPath) as! AppointmentListCell
            
            let dict = ((aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary).removeNullFromDict()
            cell.lblCName.text = "\(dict.value(forKey: "customer_name")!)" == "" ? "N/A" : "\(dict.value(forKey: "customer_name")!)"
            
            cell.lblMobileNumber.text = "\(dict.value(forKey: "customer_mobile")!)" == "" ? "N/A" : "\(dict.value(forKey: "customer_mobile")!)"
            
            cell.lblAddress.text = "\(dict.value(forKey: "address")!)" == "" ? "N/A" : "\(dict.value(forKey: "address")!)"

            cell.lblAppointmentDate.text = "\(dict.value(forKey: "slot_date")!)" == "" ? "N/A" : "\(dict.value(forKey: "slot_date")!)"

            cell.lblAppointmentTime.text = "\(dict.value(forKey: "time_slot")!)" == "" ? "N/A" : "\(dict.value(forKey: "time_slot")!)"
                
            cell.lblAppointmentAmount.text = "\(dict.value(forKey: "order_total")!)" == "\u{20B9}0.0" ? "N/A" : "\u{20B9}\(dict.value(forKey: "order_total")!)"
            
            cell.lblAssignTo.text = "\(dict.value(forKey: "assigned_to")!)" == "" ? "N/A" : "\(dict.value(forKey: "assigned_to")!)"
                 
            cell.lblCreatedDate.text = "\(dict.value(forKey: "order_date")!)" == "" ? "N/A" : "\(dict.value(forKey: "order_date")!)"
           
            cell.lblAppointmentStatus.text = "\u{2022} \(dict.value(forKey: "status")!)"
            
        
        cell.lblstaffRating.text = "\(dict.value(forKey: "order_staff_rating")!)" == "" ? "N/A" : "\(dict.value(forKey: "order_staff_rating")!)"
        cell.lblComment.text = "\(dict.value(forKey: "order_staff_comments")!)" == "" ? "N/A" : "\(dict.value(forKey: "order_staff_comments")!)"
        
            if("\(dict.value(forKey: "status")!)" == "Completed" ){
                cell.lblAppointmentStatus.textColor = UIColor.blue
            }else if("\(dict.value(forKey: "status")!)" == "Assigned" ){
                cell.lblAppointmentStatus.textColor = UIColor.orange
            }else{
                cell.lblAppointmentStatus.textColor = UIColor.darkGray
            }
            
        
        
          cell.btnExtraService.tag = indexPath.row
          cell.btnExtraService.addTarget(self, action: #selector(unFollowActionExtraService(sender:)), for: .touchUpInside)
        cell.btnCompleteService.tag = indexPath.row
        cell.btnCompleteService.addTarget(self, action: #selector(unFollowActionCompleteService(sender:)), for: .touchUpInside)
      
        if(self.segmentedControl.selectedSegmentIndex == 0){

            cell.btnExtraServiceHeight.constant = 35.0
            cell.btnCompleteServiceHeight.constant = 35.0
        }else{
            cell.btnExtraServiceHeight.constant = 0.0
            cell.btnCompleteServiceHeight.constant = 0.0
        }
            
            return cell
        
        
    
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.4) {
            cell.transform = CGAffineTransform.identity
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
             self.view.endEditing(true)
             let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryDetailVC") as? HistoryDetailVC
             vc?.dictDATA = dict.mutableCopy()as! NSMutableDictionary
             self.navigationController?.pushViewController(vc!, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryList.count == 0 ? 100 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
    @objc func unFollowActionExtraService(sender : UIButton) {
        indexSelection = sender.tag
        self.viewExtraService.frame = self.view.frame
        UIView.transition(with: self.view, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.view.addSubview(self.viewExtraService)
            
            
        }, completion: nil)
       
    }
    
    @objc func unFollowActionCompleteService(sender : UIButton) {
        indexSelection = sender.tag
        let dict = aryList.object(at: sender.tag)as! NSDictionary
        AddExtraServiceAPI(dict: dict)
    }
}
// MARK: -
// MARK: - ---------------API Calling----------

extension AssignedBookingListVC  {
    func AddExtraServiceAPI(dict : NSDictionary) {
        if(isInternetAvailable()){
         
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(dict.value(forKey: "order_id")!)", forKey: "order_id")
            dictdata.setValue("\(txtServiceName.text!)", forKey: "service_name")
            dictdata.setValue("\(txtAmountExtra.text!)", forKey: "service_amount")
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_add_billing_data) { [self] (responce, status) in
                    loader.dismiss(animated: false, completion: nil)
             
                   print(responce)
                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    self.viewExtraService.removeFromSuperview()
                    
                    
                    let alert = UIAlertController(title: "Alert", message: dict.value(forKey: "message") as? String, preferredStyle: .alert)
                    alert.view.tintColor = UIColor.black
                    alert.addAction(UIAlertAction(title: "Go to Completed List", style: .default, handler: { action in
                        self.segmentedControl.selectedSegmentIndex = 1
                        self.aryList = NSMutableArray()
                        self.tv_List.reloadData()
                        self.AssignedListAPI(forLoader: 1)
                        
                    }))
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        self.aryList = NSMutableArray()
                        self.tv_List.reloadData()
                        self.AssignedListAPI(forLoader: 1)
                        
                    }))
              
                    self.present(alert, animated: true, completion: nil)
                    
                    
                
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    
    func AssignedListAPI(forLoader : Int) {
        if(isInternetAvailable()){
            var loader = UIAlertController()
            if( forLoader == 1 ){
                 loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            } else{
                self.activityIndicator.isHidden = false
            }
            
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue("", forKey: "status")

            WebService.callAPIBYPOST(parameter: dictdata, url: URL_get_staff_orders) { [self] (responce, status) in
                if( forLoader == 1 ){
                    loader.dismiss(animated: false, completion: nil)
                } else{
                    self.activityIndicator.isHidden = true
                      self.refreshControl.endRefreshing()
                }
             
                   print(responce)
                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    self.aryList = NSMutableArray()
                    if(dict.value(forKey: "orders") != nil){
                        self.filterData(ary: (dict.value(forKey: "orders")as! NSArray).mutableCopy() as! NSMutableArray)
                    }else{
                        self.reloadTable()
                    }
                
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }

    func filterData(ary : NSMutableArray) {
        self.aryAssigned = NSMutableArray()
        self.aryCompleted = NSMutableArray()

        for item in ary {
            let dict = (item as AnyObject) as! NSDictionary
            let status = "\(dict.value(forKey: "status")!)".lowercased()
            if(status == "assigned"){
                self.aryAssigned.add(dict)
            }else if(status == "completed"){
                self.aryCompleted.add(dict)
            }
        }
        reloadTable()
    }
    
}
// MARK:
// MARK:- -------------UITextFieldDelegate----------

extension AssignedBookingListVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtAmountExtra){
            return TextValidation(textField: textField, string: string, returnOnly: "DECIMEL", limitValue: 9)
        }
        else {
            return TextValidation(textField: textField, string: string, returnOnly: "", limitValue: 45)
        }
    }
}

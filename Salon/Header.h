//
//  Header.h
//  Salon
//
//  Created by NavinPatidar on 10/5/20.
//  Copyright © 2020 Salon. All rights reserved.
//

#ifndef Header_h
#define Header_h

#import <CommonCrypto/CommonCrypto.h>
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#endif /* Header_h */

//
//  HistoryVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/9/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController {

    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var aryList = NSMutableArray()
    var refreshControl = UIRefreshControl()
    // MARK: - ------LifeCycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        tv_List.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
         refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
         tv_List.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true
        if(aryList.count == 0){
             self.HistoryListAPI()
        }else{
            self.tv_List.reloadData()
            self.tv_List.delegate = self
            self.tv_List.dataSource = self
        }
    }
    @objc func refresh(_ sender: AnyObject) {
        self.HistoryListAPI()
    }

  
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension HistoryVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "HistoryListCell" : "HistoryListCell", for: indexPath as IndexPath) as! HistoryListCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        
        cell.lblBookingID.text = "\(dict.value(forKey: "order_name")!)"
        cell.lblStatus.text = "\u{2022} \(dict.value(forKey: "status")!)"
        if("\(dict.value(forKey: "status")!)" == "Completed" ){
            cell.lblStatus.textColor = UIColor.blue
        }else if("\(dict.value(forKey: "status")!)" == "Assigned" ){
            cell.lblStatus.textColor = UIColor.orange
        }else{
            cell.lblStatus.textColor = UIColor.darkGray
        }
        
        cell.lblAMount.text = "\u{20B9}\(dict.value(forKey: "order_total")!)"
        cell.lblAppointmentDate.text = "\(dict.value(forKey: "slot_date")!)"
        cell.lblAppointmentTime.text = "\(dict.value(forKey: "time_slot")!)"
        cell.lblAppointmentCreatedDate.text = "\(dict.value(forKey: "order_date")!)"
        cell.btnRateStaff.tag = indexPath.row
        cell.btnRateStaff.addTarget(self, action: #selector(unFollowAction), for: .touchUpInside)
        
        if("\(dict.value(forKey: "status")!)" == "Completed"){
            if("\(dict.value(forKey: "order_staff_rating")!)" == ""){
                cell.btnRateStaffHeight.constant = 35.0
                cell.lblStafrating.text = ""
                cell.lblComments.text = ""
                cell.staticlblStafrating.text = ""
                cell.staticlblComments.text  = ""
                
            }else{
                cell.staticlblStafrating.text = "Staff Rating-"
                cell.staticlblComments.text  = "Comment-"
                cell.lblStafrating.text = "\(dict.value(forKey: "order_staff_rating")!)"
                cell.lblComments.text = "\(dict.value(forKey: "order_staff_comments")!)"
                cell.btnRateStaffHeight.constant = 0.0

            }
        }else{
            cell.btnRateStaffHeight.constant = 0.0
            cell.lblStafrating.text = ""
            cell.lblComments.text = ""
            cell.staticlblStafrating.text = ""
            cell.staticlblComments.text  = ""
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
         self.view.endEditing(true)
         let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryDetailVC") as? HistoryDetailVC
         vc?.dictDATA = dict.mutableCopy()as! NSMutableDictionary
         self.navigationController?.pushViewController(vc!, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryList.count == 0 ? 100 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
 
    @objc func unFollowAction(sender : UIButton) {
       let dict = aryList.object(at: sender.tag)as! NSDictionary
        self.view.endEditing(true)
        scaleAnimationONUIButton(sender: sender)
        self.view.endEditing(true)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "FeedBackVC") as? FeedBackVC
        vc?.strComeFrom = "History"
        vc?.dictDAta = dict.mutableCopy()as! NSMutableDictionary
        vc?.delegate = self
        self.navigationController?.pushViewController(vc!, animated: true)
      
    }
}
// MARK: -
// MARK: ----------ReviewListCell---------
class HistoryListCell: UITableViewCell {
    @IBOutlet weak var lblBookingID: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAMount: UILabel!
    @IBOutlet weak var lblAppointmentDate: UILabel!
    @IBOutlet weak var lblAppointmentTime: UILabel!
    @IBOutlet weak var lblAppointmentCreatedDate: UILabel!
    @IBOutlet weak var lblStafrating: UILabel!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var staticlblStafrating: UILabel!
    @IBOutlet weak var staticlblComments: UILabel!
    @IBOutlet weak var btnRateStaff: UIButton!
    @IBOutlet weak var btnRateStaffHeight: NSLayoutConstraint!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: -
// MARK: - ---------------API Calling----------

extension HistoryVC  {
    func HistoryListAPI() {
        if(isInternetAvailable()){
         self.activityIndicator.isHidden = false
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_get_orders) { (responce, status) in
              self.activityIndicator.isHidden = true
                self.refreshControl.endRefreshing()
  print(responce)
                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    self.aryList = NSMutableArray()
                    if(dict.value(forKey: "orders") != nil){
                        self.aryList = (dict.value(forKey: "orders")as! NSArray).mutableCopy() as! NSMutableArray
                      
                    }
                    self.tv_List.reloadData()
                    self.tv_List.delegate = self
                    self.tv_List.dataSource = self
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
}
// MARK: -
// MARK: - ------------FeedbackDelegate-------

extension HistoryVC : FeedbackDelegate{
    func refreshScreen() {
        self.HistoryListAPI()
    }
    
    
}

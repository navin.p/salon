//
//  AppoinmentVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/6/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class AppoinmentVC: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
    var dictDetail = NSMutableDictionary()

    var aryList = NSMutableArray()
    var refreshControl = UIRefreshControl()
    
    
    @IBOutlet weak var viewAppointment: UIView!
    @IBOutlet weak var txtNumber: ACFloatingTextfield!
    @IBOutlet weak var txtdate: ACFloatingTextfield!
    @IBOutlet weak var txtSlot: ACFloatingTextfield!
    @IBOutlet weak var txtAddress: KMPlaceholderTextView!

    // MARK: - ------LifeCycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        tv_List.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tv_List.addSubview(refreshControl)
        txtNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true
        
        if(aryList.count == 0){
            self.AppointmentListAPI()
        }else{
           
                self.tv_List.reloadData()
                self.tv_List.delegate = self
                self.tv_List.dataSource = self
        }
    }
    @objc func refresh(_ sender: AnyObject) {
        self.AppointmentListAPI()
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let text = textField.text ?? ""
        var trimmedText = text.trimmingCharacters(in: .whitespaces)
        trimmedText = (trimmedText.replacingOccurrences(of: " ", with: ""))
        textField.text = trimmedText
        if(txtNumber == textField){
            textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
        }
    }

    // MARK: - ------IBAction----------
    // MARK: -

    @IBAction func actionOnMenu(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        
        let nextVc: DrawerVC = mainStoryboard.instantiateViewController(withIdentifier: "DrawerVC") as! DrawerVC
        nextVc.handleDrawerView = self
        nextVc.modalPresentationStyle = .overCurrentContext
        nextVc.modalTransitionStyle = .crossDissolve
        self.present(nextVc, animated: false, completion: nil)
        
    }
    @IBAction func actionOnTImeSlot(_ sender: UIButton) {
        self.view.endEditing(true)
        let vc: SelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "SelectionClass") as! SelectionClass
        vc.strTag = 1
        let temp = NSMutableArray()
        var dict = NSMutableDictionary()
        dict.setValue("10:00 AM - 10:30 AM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("10:30 AM - 11:00 AM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("11:00 AM - 11:30 AM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("11:30 AM - 12:30 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("12:30 PM - 1:00 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("1:00 PM - 1:30 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("1:30 PM - 2:00 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("2:00 PM - 2:30 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("2:30 PM - 3:00 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("3:00 PM - 3:30 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("3:30 PM - 4:00 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("4:00 PM - 4:30 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("4:30 PM - 5:00 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("5:00 PM - 5:30 PM", forKey: "name")
        temp.add(dict)
        dict = NSMutableDictionary()
        dict.setValue("5:30 PM - 6:00 PM", forKey: "name")
        temp.add(dict)
        
        vc.aryList = temp
        vc.aryForSelectedListData = NSMutableArray()
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        vc.delegate = self
        self.present(vc, animated: true, completion: {})
    }
    @IBAction func actionOnDate(_ sender: UIButton) {
        self.view.endEditing(true)
        self.view.endEditing(true)
                 let vc: DateTimeSelectionClass = mainStoryboard.instantiateViewController(withIdentifier: "DateTimeSelectionClass") as! DateTimeSelectionClass
                 vc.strMode = "Date"
                 vc.strTag = 1
                 vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                 vc.modalTransitionStyle = .coverVertical
                 vc.delegateDateTime = self
                 self.present(vc, animated: true, completion: {})
    }
    @IBAction func actionOnSubmit(_ sender: UIButton) {
        self.view.endEditing(true)
        scaleAnimationONUIButton(sender: sender)
     
        if(validationForAppointment()){
            self.BookAppointmentAPI()
        }
    }
   
    @IBAction func actionOnBack(_ sender: UIButton) {
        self.view.endEditing(true)
        viewAppointment.removeFromSuperview()
    }
    @IBAction func actionOnBottomButton(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.view.endEditing(true)
        var count = 0
        if(sender.tag == 0){ // Service
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DashBoardVC.self) {
                    count = 1
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            if(count == 0){
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
        else if(sender.tag == 1){ //Offer
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: Offervc.self) {
                    count = 1
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            if(count == 0){
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "Offervc") as? Offervc
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
        else if(sender.tag == 2){ //Appoinment
         
        }
        else if(sender.tag == 3){ //Profile
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: MyProfileVC.self) {
                    count = 1
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            if(count == 0){
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
        
    }

}
// MARK: - ------------DrawerScreenDelegate---------
// MARK: -
extension AppoinmentVC: DrawerScreenDelegate {
    func refreshDrawerScreen(strType: String, tag: Int) {
            if(strType == "Sign out"){
                       self.view.endEditing(true)
                       let alert = UIAlertController(title: "Sign out", message: alertLogout, preferredStyle: .alert)
                       alert.view.tintColor = UIColor.black
                       let Yes = (UIAlertAction(title: "Yes", style: .default , handler:{ (UIAlertAction)in
                        self.LogOutAPI()
                       }))
                       alert.addAction(Yes)
                   
                       alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                           print("User click Add Cancel  button")
                       }))
                       
                       
                       if let popoverController = alert.popoverPresentationController {
                           popoverController.sourceView = self.view
                           popoverController.sourceRect = self.view.bounds
                           popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                       }
                       
                       self.present(alert, animated: true, completion: {
                           print("completion block")
                       })
                       
                   }
                   
                   
                   else if (strType == "Change Password"){
                       self.view.endEditing(true)
                           let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC
                           self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Feedback"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "FeedBackVC") as? FeedBackVC
                self.navigationController?.pushViewController(vc!, animated: true)
        }
        else if (strType == "About Us"){
                      self.view.endEditing(true)
                      let vc = mainStoryboard.instantiateViewController(withIdentifier: "AboutUSVC") as? AboutUSVC
                      self.navigationController?.pushViewController(vc!, animated: true)
              }
        else if (strType == "Contact Us"){
                      self.view.endEditing(true)
                      let vc = mainStoryboard.instantiateViewController(withIdentifier: "ContactUSVC") as? ContactUSVC
                      self.navigationController?.pushViewController(vc!, animated: true)
              }
        else if (strType == "Assigned Booking List"){
                            self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AssignedBookingListVC") as? AssignedBookingListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Glimpses of Gallery"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "GlimpsesofGalleryVC") as? GlimpsesofGalleryVC
                self.navigationController?.pushViewController(vc!, animated: true)
        }
            else if (strType == "History"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryVC") as? HistoryVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }  else if (strType == "Review"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ReviewListVC") as? ReviewListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }  else if (strType == "Appointment List"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentListVC") as? AppointmentListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Staff List"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "StaffListVC") as? StaffListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Profile"){ //Profile
                var count = 0
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: MyProfileVC.self) {
                        count = 1
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                if(count == 0){
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
            }
            else if (strType == "Home"){
                self.view.endEditing(true)
                var count = 0
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: DashBoardVC.self) {
                        count = 1
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                if(count == 0){
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
         
            }
            else if (strType == "Login"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                vc!.view.tag = 1

                self.navigationController?.pushViewController(vc!, animated: false)
         
            }
            else if (strType == "Signup"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                vc!.view.tag = 2

                self.navigationController?.pushViewController(vc!, animated: false)
         
            }
    }
    
    
    
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension AppoinmentVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? self.aryList.count : self.aryList.count == 0 ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
            let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "OfferCell" : "OfferCell", for: indexPath as IndexPath) as! OfferCell
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            cell.lblTitle.text = "\(dict.value(forKey: "name")!)"
            cell.lblDisAmmount.text = ""
            cell.lblAmmount.text = "\u{20B9}\(dict.value(forKey: "product_subtotal")!)"
            
            cell.imgView.image = UIImage(named: "salon_on_wheels")
            cell.imgView.layer.cornerRadius =  6.0
            cell.imgView.layer.masksToBounds = false
            cell.imgView.clipsToBounds = true
            
            let urlImage = "\(dict.value(forKey: "img_url")!)"
            cell.imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "salon_on_wheels"), options: SDWebImageOptions(rawValue: 0), progress: { (S1, S2) in
            }, completed: { (IMAGE, ERROR, cacheType, imageURL) in
                
            }, usingActivityIndicatorStyle: .gray)
            
   
            return cell
        }else{
            let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "OfferCellPayment" : "OfferCellPayment", for: indexPath as IndexPath) as! OfferCell
            cell.lblConvenFee.text = "\u{20B9}\(self.dictDetail.value(forKey: "convenience_fee")!)"
            cell.lblTotalamount.text = "\u{20B9}\(self.dictDetail.value(forKey: "total")!)"
            cell.lblGrandTotal.text = "\u{20B9}\(self.dictDetail.value(forKey: "total")!)"
            cell.lblDisamount.text = "-\u{20B9}0.0"
            if(self.dictDetail.value(forKey: "discounted_total") != nil){
                cell.lblGrandTotal.text = "\u{20B9}\(self.dictDetail.value(forKey: "discounted_total")!)"
            }
            if(self.dictDetail.value(forKey: "promocode") != nil){
                let dictPromocode = self.dictDetail.value(forKey: "promocode")as! NSDictionary
                let type = "\(dictPromocode.value(forKey: "discount_type")!)"
                let discount_amount = "\(dictPromocode.value(forKey: "discount_amount")!)"
                let a = ("\(self.dictDetail.value(forKey: "total")!)"as NSString).floatValue
                let b = ("\(dictDetail.value(forKey: "discounted_total")!)"as NSString).floatValue

                if(type == "percent"){
                    cell.lblDisamount.text = "-\u{20B9}\(a - b)  (\(discount_amount)%)"
                }else{
                    cell.lblDisamount.text = "-\u{20B9}\(discount_amount)"
                }
            }
            
            
            let aa = ("\(cell.lblGrandTotal.text!)".replacingOccurrences(of: "\u{20B9}", with: "") as NSString).floatValue
            let bb = ("\(cell.lblConvenFee.text!)".replacingOccurrences(of: "\u{20B9}", with: "") as NSString).floatValue
            cell.lblGrandTotal.text = "\u{20B9}\(aa + bb)"
            cell.btnBook.setTitle("BOOK APPOINTMENT: \(cell.lblGrandTotal.text!)", for: .normal)
            cell.btnBook.tag = indexPath.row
            cell.btnBook.addTarget(self, action: #selector(unFollowAction), for: .touchUpInside)
            cell.btnPromocode.tag = indexPath.row
            cell.btnPromocode.addTarget(self, action: #selector(unPromocdeAction), for: .touchUpInside)
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? UITableView.automaticDimension : 285.0

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryList.count == 0 ? section == 0 ? 100 : 0 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0 ? true : false
    }

     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            self.RemoveProductAPI(key: "\(dict.value(forKey: "cart_key")!)")
        }
    }
   
    @objc func unFollowAction(sender : UIButton) {
      //  let dict = aryList.object(at: sender.tag)as! NSDictionary
        self.view.endEditing(true)
        scaleAnimationONUIButton(sender: sender)
        self.viewAppointment.frame = self.view.bounds
        self.view.addSubview(self.viewAppointment)
        self.viewAppointment.transform = CGAffineTransform.init(scaleX: 0.6, y: 0.6)
            UIView.animate(withDuration: 0.4, animations: { () -> Void in
                self.viewAppointment.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            })
    }
    @objc func unPromocdeAction(sender : UIButton) {
        //let dict = aryList.object(at: sender.tag)as! NSDictionary
        scaleAnimationONUIButton(sender: sender)
        self.view.endEditing(true)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "PromoCodeVC") as? PromoCodeVC
        vc!.delegate = self
        self.present(vc!, animated: true, completion: nil)

    }
}

// MARK: -
// MARK: - ---------------API Calling----------

extension AppoinmentVC  {
    func BookAppointmentAPI() {
         if(isInternetAvailable()){
             let dictdata = NSMutableDictionary()
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "customer_id")
            dictdata.setValue("Cash", forKey: "gateway")
            dictdata.setValue("\(txtdate.text!)", forKey: "date")
            dictdata.setValue("\(txtAddress.text!)", forKey: "address")
            dictdata.setValue("\(txtNumber.text!)", forKey: "mobile")
            dictdata.setValue("Pending", forKey: "status")
            dictdata.setValue("0", forKey: "transaction_id")
            dictdata.setValue("\(txtSlot.text!)", forKey: "time_slot")
            dictdata.setValue("\(self.dictDetail.value(forKey: "convenience_fee")!)", forKey: "convince_fee")
           
            if(self.dictDetail.value(forKey: "promocode") != nil){
                let dictPromocode = self.dictDetail.value(forKey: "promocode")as! NSDictionary
                dictdata.setValue("\(dictPromocode.value(forKey: "coupon_id")!)", forKey: "promo_id")
            }
            dictdata.setValue("", forKey: "promo_id")

            let aryList = NSMutableArray()
            for item in self.aryList {
                let dictSend = NSMutableDictionary()
                let dict = ((item as AnyObject) as! NSDictionary).mutableCopy()as! NSMutableDictionary
                dictSend.setValue("\(dict.value(forKey: "product_id")!)", forKey: "product_id")
                dictSend.setValue("\(dict.value(forKey: "quantity")!)", forKey: "quantity")
                dictSend.setValue("\(dict.value(forKey: "variation_id")!)", forKey: "variation_id")
                aryList.add(dictSend)
            }
            dictdata.setValue(aryList, forKey: "line_items")

            var json = Data()
            var jsonString = NSString()
            if JSONSerialization.isValidJSONObject(dictdata) {
                // Serialize the dictionary
                json = try! JSONSerialization.data(withJSONObject: dictdata, options: .prettyPrinted)
                jsonString = String(data: json, encoding: .utf8)! as NSString
                print("UpdateLeadinfo JSON: \(jsonString)")
            }
            dictdata.setValue(jsonString, forKey: "order_items")
            print(" JSON: \(dictdata)")

             WebService.callAPIBYPOST(parameter: dictdata, url: URL_create_order) { (responce, status) in
                 if (status){
                    print(responce)
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary

                    loader.dismiss(animated: false, completion: nil)
                    if("\(dict.value(forKey: "error")!)" == "1"){
                       showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        self.view.endEditing(true)
                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ThankYouVC") as? ThankYouVC
                        vc?.dictDetail = (dict.value(forKey: "order_data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }

                 }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)
                 }
             }
         }
         else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
         }
     }
    func RemoveProductAPI(key : String) {
         if(isInternetAvailable()){
             let dictdata = NSMutableDictionary()
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue(key, forKey: "cart_key")
        
             WebService.callAPIBYPOST(parameter: dictdata, url: URL_delete_from_cart) { (responce, status) in
                 if (status){
                    print(responce)
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary

                    loader.dismiss(animated: false, completion: nil)
                    if("\(dict.value(forKey: "error")!)" == "1"){
                       showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        self.AppointmentListAPI()

                    }

                 }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)
                 }
             }
         }
         else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
         }
     }
    func AppointmentListAPI() {
         if(isInternetAvailable()){
             let dictdata = NSMutableDictionary()
          //  let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue("get_cart", forKey: "request")
            self.activityIndicator.isHidden = false

             WebService.callAPIBYPOST(parameter: dictdata, url: URL_get_cart) { (responce, status) in
                self.activityIndicator.isHidden = true
                  self.refreshControl.endRefreshing()
                 if (status){
                    print(responce)
                   // loader.dismiss(animated: false, completion: nil)
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    
                    self.dictDetail = dict.mutableCopy()as! NSMutableDictionary
                    self.aryList = NSMutableArray()
                    if(dict.value(forKey: "products") != nil){
                        self.aryList = (dict.value(forKey: "products")as! NSArray).mutableCopy() as! NSMutableArray
                      
                    }
                    self.tv_List.reloadData()
                    self.tv_List.delegate = self
                    self.tv_List.dataSource = self

                 }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)
                 }
             }
         }
         else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
         }
     }
    func LogOutAPI() {
         if(isInternetAvailable()){
             let dictdata = NSMutableDictionary()
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)

            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
             WebService.callAPIBYPOST(parameter: dictdata, url: URL_LogOut) { (responce, status) in
                 if (status){
                    loader.dismiss(animated: false, completion: nil)
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary

                    
                    if("\(dict.value(forKey: "error")!)" == "1"){
                        showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        UserDefaults.standard.synchronize()
                        globleLogInData = NSMutableDictionary()

                        self.view.endEditing(true)
                        let redViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as! LoginSigUpVC
                        let navigationController = UINavigationController(rootViewController: redViewController)

                        UIApplication.shared.windows.first?.rootViewController = navigationController
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
                        navigationController.setNavigationBarHidden(true, animated: true)
                    }
                    
                 }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                 }
             }
         }
         else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
         }
     }
}
//MARK:-
//MARK:- PopUpDelegate
extension AppoinmentVC : PopUpDelegate{
    func getDataFromPopupDelegateArray(aryData: NSArray, tag: Int) {
        
    }
    
    func getDataFromPopupDelegate(dictData: NSDictionary, tag: Int) {
        if(dictData.count != 0){
            txtSlot.text = "\(dictData.value(forKey: "name")!)"
        }else{
            txtSlot.text = ""
        }

    }
}
//MARK:-
//MARK:- DateTimeDelegate
extension AppoinmentVC : DateTimeDelegate{
    func getDataFromDelegate(time: Date, tag: Int) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
              if(tag == 1){
                  self.txtdate.text = "\(dateFormatter.string(from: time))"
              }
    }
  
}
// MARK:
// MARK:- Validation

extension AppoinmentVC {
    func validationForAppointment() -> Bool {
        
        if(txtdate.text?.count == 0){
            addErrorMessage(textView: txtdate, message: alert_required)
            return false
        }
        else if(txtSlot.text?.count == 0){
            addErrorMessage(textView: txtSlot, message: alert_required)
            return false
        }
        else if(txtNumber.text?.count == 0){
            addErrorMessage(textView: txtNumber, message: alert_required)
            return false
        }
        else if(txtNumber.text!.count < 10){
            addErrorMessage(textView: txtNumber, message: alert_MobileNumberValid)
            return false
        }
        else if(txtAddress.text!.count == 0){
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: "Address is required!", viewcontrol: self)
            return false
        }
        return true
    }
   
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK:
// MARK:- PromocodeDelegate
extension AppoinmentVC : PromocodeDelegate{
    func getDataFromPromocodeDelegate(dictData: NSDictionary, tag: Int) {
        self.dictDetail = dictData.mutableCopy()as! NSMutableDictionary
        self.tv_List.reloadData()
    }
}
// MARK:
// MARK:- -------------UITextFieldDelegate----------

extension AppoinmentVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtNumber){
            return TextValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        else {
            return TextValidation(textField: textField, string: string, returnOnly: "", limitValue: 45)
        }
    }
}

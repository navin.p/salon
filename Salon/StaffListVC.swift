//
//  StaffListVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/7/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class StaffListVC: UIViewController {
   var strComeFrom = ""
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var lbltitle: UILabel!
    var aryList = NSMutableArray()
    var refreshControl = UIRefreshControl()
 
    // MARK: - ------LifeCycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        tv_List.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
         refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
         tv_List.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true
        if(aryList.count == 0){
             self.StaffListVCListAPI()
        }else{
            self.tv_List.reloadData()
            self.tv_List.delegate = self
            self.tv_List.dataSource = self
        }
    }
    @objc func refresh(_ sender: AnyObject) {
        self.StaffListVCListAPI()
    }
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension StaffListVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "StaffCell" : "StaffCell", for: indexPath as IndexPath) as! StaffCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        cell.lblName.text = "\(dict.value(forKey: "name")!)"
        cell.btnMobile.setTitle("\(dict.value(forKey: "phone")!)", for: .normal)
        cell.btnMobile.tag = indexPath.row
        cell.btnMobile.addTarget(self, action: #selector(unFollowAction), for: .touchUpInside)

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
      
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryList.count == 0 ? 100 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
    
    @objc func unFollowAction(sender : UIButton) {
        print("Button wurde gedrückt")
        let dict = aryList.object(at: sender.tag)as! NSDictionary

        if !(callingFunction(number: "\(dict.value(forKey: "phone")!)" as NSString)){
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: alertCalling, viewcontrol: self)
        }
    }
}
// MARK: -
// MARK: ----------StaffCell---------
class StaffCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnMobile: UIButton!
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: -
// MARK: - ---------------API Calling----------

extension StaffListVC  {
    func StaffListVCListAPI() {
        if(isInternetAvailable()){
         self.activityIndicator.isHidden = false
            let dictdata = NSMutableDictionary()
            WebService.callAPIBYGET(parameter: dictdata, url: URL_get_all_staff) { (responce, status) in
              self.activityIndicator.isHidden = true
                self.refreshControl.endRefreshing()

                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    self.aryList = NSMutableArray()
                    if(dict.value(forKey: "staffs") != nil){
                        self.aryList = (dict.value(forKey: "staffs")as! NSArray).mutableCopy() as! NSMutableArray
                      
                    }
                    self.tv_List.reloadData()
                    self.tv_List.delegate = self
                    self.tv_List.dataSource = self
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
}

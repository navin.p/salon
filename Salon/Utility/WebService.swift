//
//  WebService.swift
//  AlamoFire_WebService
//
//  Created by admin on 22/12/16.
//  Copyright © 2016 admin. All rights reserved.
//

import UIKit
import Alamofire

var InstagramLink :String = "https://instagram.com/salononwheels_ind?igshid=9sad240mf0jy"
var FacebookLink :String = "https://www.facebook.com/Salononwheelsindore/"
var CallNumber :String = "9713536000"


var MainBaseURL :String = "http://projects.stagingsoftware.com/salonapp/"

var BaseURL :String = "http://projects.stagingsoftware.com/salonapp/info-extended/api/"
   var URL_DashBoard :String = BaseURL + "dashboard/"
var URL_Login :String = BaseURL + "login/"
var URL_register :String = BaseURL + "register/"
var URL_LogOut :String = BaseURL + "logout/"
var URL_ForgotPAss :String = BaseURL + "forgot_password/"
var URL_OtpVerify :String = BaseURL + "verify_otp/"
var URL_OtpResend :String = BaseURL + "resend_otp/"
var URL_get_all_staff :String = BaseURL + "get_all_staff/"
var URL_get_product_by_cat :String = BaseURL + "get_product_by_cat/"

var URL_get_product_detail :String = BaseURL + "get_product_detail/"
var URL_add_cart :String = BaseURL + "add_cart/"
var URL_get_cart :String = BaseURL + "get_cart/"
var URL_delete_from_cart :String = BaseURL + "delete_from_cart/"
var URL_get_promocodes :String = BaseURL + "get_promocodes/"
var URL_apply_promocode :String = BaseURL + "apply_promocode/"

var URL_get_feedbacks :String = BaseURL + "get_feedbacks/"

var URL_create_order :String = BaseURL + "create_order/"
var URL_aboutus :String = MainBaseURL + "about-us/"

var URL_change_password :String = BaseURL + "change_password/"
var URL_get_orders :String = BaseURL + "get_orders/"
var URL_staff_feedback :String = BaseURL + "staff_feedback/"
var URL_feedback :String = BaseURL + "feedback/"

var URL_get_order_by_id :String = BaseURL + "get_order_by_id/"
var URL_update_profile_pic :String = BaseURL + "update_profile_pic/"
var URL_get_products :String = BaseURL + "get_products/"
var URL_get_Allorders :String = BaseURL + "get_all_orders/"

var URL_assign_booking :String = BaseURL + "assign_booking/"
var URL_get_staff_orders :String = BaseURL + "get_staff_orders/"
var URL_add_billing_data :String = BaseURL + "add_billing_data/"

var URL_check_user_device :String = BaseURL + "check_user_device/"



@objc class WebService: NSObject,NSURLConnectionDelegate {
    class func callAPIBYGET(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        AF.request(strUrlwithString!, method: .get, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,true)
                }
                break
            case .failure(_):
                print(response.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic .setValue("\(alertSomeError) ", forKey: "message")
                OnResultBlock(dic,false)
                break
            }
        }
    }
        
    class func callAPIBYPOST(parameter:NSDictionary,url:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void) {
        let strUrlwithString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        AF.request(strUrlwithString!, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:AFDataResponse<Any>) in
            switch(response.result) {
            case .success(_):
                if let data = response.value
                {
                    // print(data)
                    let dictdata = NSMutableDictionary()
                    dictdata.setValue(data, forKey: "data")
                    OnResultBlock((dictdata) ,true)
                }
                break
            case .failure(_):
                print(response.error ?? 0)
                let dic = NSMutableDictionary.init()
                dic.setValue("Connection Time Out ", forKey: "message")
                OnResultBlock(dic,false)
                break
            }
        }
    }
    
        class func callAPIWithImage(parameter:NSDictionary,url:String,image:UIImage,fileName:String,withName:String,OnResultBlock: @escaping (_ dict: NSDictionary,_ status:Bool) -> Void){
            
                    AF.upload(multipartFormData: { (multipartdata) in

                        let imageData = image.jpegData(compressionQuality: 0.50)
                        multipartdata.append(imageData!, withName: fileName, fileName: "\(withName).png", mimeType: "image/png")
                        for (key, value) in parameter {
                            multipartdata.append((value as! String).data(using: String.Encoding.utf8)!, withName: key as! String)
                        }
                        
                    }, to: url).responseJSON { response in
                       print(response.request ?? 0)  // original URL request
                        print(response.response ?? 0) // URL response
                        print(response.data ?? 0)     // server data
                        print(response.result)   // result of response serialization
                        
                        if let data = response.value
                        {
                            let dictdata = NSMutableDictionary()
                            dictdata.setValue(data, forKey: "data")
                            OnResultBlock((dictdata) ,true)
                        }else{
                            let dic = NSMutableDictionary.init()
                            dic .setValue("\(alertSomeError) ", forKey: "message")
                            OnResultBlock(dic,false)
                        }
            }
            
    }

    
    //MARK:
    //MARK: Get Device ID
    func get_DeviceID() -> String {
        let device = UIDevice.current
        let str_deviceID = device.identifierForVendor?.uuidString
        return str_deviceID!
    }
    
    //MARK:
    //MARK: Get Current Time
    func get_currentTime() -> String
    {
        let timeFormat = DateFormatter.init()
        timeFormat.dateFormat = "mmddHHmmss"
        return timeFormat.string(from: Date.init())
        
    }
    
  
}

//
//  FeedBackVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/6/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit
//MARK:
//MARK: ---------------Protocol-----------------
protocol FeedbackDelegate : class{
    func refreshScreen()
}
class FeedBackVC: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var txtView: KMPlaceholderTextView!
    @IBOutlet weak var floatRatingView: FloatRatingView!
    @IBOutlet var updatedLabel: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblTitleMessage: UILabel!
    var strComeFrom = ""
    var dictDAta = NSMutableDictionary()
    weak var delegate: FeedbackDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        floatRatingView.delegate = self
        floatRatingView.contentMode = UIView.ContentMode.scaleAspectFit
        floatRatingView.type = .halfRatings
        
        if(strComeFrom == "History"){
            lblTitle.text = "Rating to staff"
            lblTitleMessage.text = "How was your experiance with staff?"
        }else{
            lblTitle.text = "Feedback"
            lblTitleMessage.text = "How was your experiance?"
        }
        
    }
    

    // MARK: - ------IBAction----------
      // MARK: -
      @IBAction func actionBack(_ sender: UIButton) {
          scaleAnimationONUIButton(sender: sender)
          
          self.navigationController?.popViewController(animated: true)
      }
    @IBAction func actionsubmit(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)

        self.view.endEditing(true)
        if txtView.text == "" {
            showAlertWithoutAnyAction(strtitle: alertMsg, strMessage: "Feedback is required!", viewcontrol: self)
        }else{
            if(strComeFrom == "History"){
                self.StaffRatingBackAPI()
            }else{
                FeedBackAPI()
            }
           
        }
    }
}
// MARK: FloatRatingViewDelegate

extension FeedBackVC: FloatRatingViewDelegate {

    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        updatedLabel.text = String(format: "%.1f", self.floatRatingView.rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        updatedLabel.text = String(format: "%.1f", self.floatRatingView.rating)
    }
    
}

// MARK: -
// MARK: - ---------------API Calling----------

extension FeedBackVC  {
    func FeedBackAPI() {
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue("\(floatRatingView.rating)", forKey: "rating")
            dictdata.setValue("\(txtView.text!)", forKey: "comments")
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_feedback) { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    if("\(dict.value(forKey: "error")!)" == "1"){
                       showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        showToast(message: "\(dict.value(forKey: "message")!)", font: self.updatedLabel.font , viewww: self.view)
                            self.navigationController?.popViewController(animated: false)
                    }
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func StaffRatingBackAPI() {
        if(isInternetAvailable()){
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)

            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
            dictdata.setValue("\(self.dictDAta.value(forKey: "order_id")!)", forKey: "order_id")
            dictdata.setValue("\(self.dictDAta.value(forKey: "assigned_id")!)", forKey: "staff_id")
            dictdata.setValue("\(floatRatingView.rating)", forKey: "rating")
            dictdata.setValue("\(txtView.text!)", forKey: "comments")

     
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_staff_feedback) { [self] (responce, status) in
                loader.dismiss(animated: false, completion: nil)

                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                   
                    if("\(dict.value(forKey: "error")!)" == "1"){
                       showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        showToast(message: "\(dict.value(forKey: "message")!)", font: self.updatedLabel.font , viewww: self.view)
                        self.delegate?.refreshScreen()
                            self.navigationController?.popViewController(animated: false)

                    }
            
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
}

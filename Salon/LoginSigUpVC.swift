//
//  LoginSigUpVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/1/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class LoginSigUpVC: UIViewController {
    var imagePicker = UIImagePickerController()
    var strIMagename = ""
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewIndicateLOGIN: CardView!
    
    @IBOutlet weak var viewIndicateSIGNUP: CardView!
    
    //MARK: --------------Login-----------
    @IBOutlet weak var txtMobileNumber: ACFloatingTextfield!
    
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    
    @IBOutlet weak var btnPassShowHide: UIButton!
    
    @IBOutlet weak var btnRemember: UIButton!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var heightLoginView: NSLayoutConstraint!
    //MARK: --------------SIGNUP--------------
    
    @IBOutlet weak var heightSignUpView: NSLayoutConstraint!
    @IBOutlet weak var btnback: UIButton!

    @IBOutlet weak var btnSignUP: UIButton!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtSignMobile: ACFloatingTextfield!
    @IBOutlet weak var txtSignEmail: ACFloatingTextfield!
    @IBOutlet weak var txtSignPass: ACFloatingTextfield!
    @IBOutlet weak var txtSignUserName: ACFloatingTextfield!
    
    //MARK: --------------LifeCycle------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipes(_:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        view.addGestureRecognizer(leftSwipe)
        view.addGestureRecognizer(rightSwipe)
        self.view.tag = 1
        txtMobileNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        txtSignMobile.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        
        PushNotiManagerVC().registerForPushNotifications()
        if(nsud.value(forKey: "fcmToken") != nil){
            fcm_token = "\(nsud.value(forKey: "fcmToken")!)"
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imgProfile.layer.cornerRadius = 42.0
        imgProfile.layer.borderWidth = 1.0
        imgProfile.layer.borderColor = UIColor.lightGray.cgColor
        switchViewAccordingCOnditions()
    }
    //MARK: --------------Extra Function------------
    
    func switchViewAccordingCOnditions()  {
        
        
        if(self.view.tag == 1){
            btnback.isHidden = true
            heightLoginView.constant = 450.0
            heightSignUpView.constant = 0.0
            lblTitle.text = "LOGIN"
            viewIndicateLOGIN.backgroundColor = hexStringToUIColor(hex: primaryYelloColor)
            viewIndicateSIGNUP.backgroundColor = UIColor.lightGray
            viewIndicateSIGNUP.layoutIfNeeded()
            txtMobileNumber.text = ""
            txtPassword.text = ""
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }else{
            heightSignUpView.constant = 500.0
            heightLoginView.constant = 0.0
            lblTitle.text = "REGISTRATION"
            viewIndicateSIGNUP.backgroundColor = hexStringToUIColor(hex: primaryYelloColor)
            viewIndicateLOGIN.backgroundColor = UIColor.lightGray
            viewIndicateLOGIN.layoutIfNeeded()
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutIfNeeded()
            })
        }
    }
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .left) {
            print("Swipe Left")
            self.view.tag = 2
            switchViewAccordingCOnditions()
            btnback.isHidden = false
        }
        if (sender.direction == .right) {
            print("Swipe Right")
            self.view.tag = 1
            switchViewAccordingCOnditions()
            btnback.isHidden = true
            
        }
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let text = textField.text ?? ""
        
        var trimmedText = text.trimmingCharacters(in: .whitespaces)
        trimmedText = (trimmedText.replacingOccurrences(of: " ", with: ""))
        
        textField.text = trimmedText
        if(txtMobileNumber == textField){
            textField.text = textField.text?.replacingOccurrences(of: "-", with: "")
        }
    }
    //MARK: --------------Action LOGIN view------------
    @IBAction func actionONSkip(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.view.endEditing(true)
        var count = 0
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                count = 1
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
        if(count == 0){
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
            self.navigationController?.pushViewController(vc!, animated: false)
        }
       
        
       }
    @IBAction func actionONNewUSer(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
                    txtSignUserName.text = ""
                    txtSignEmail.text = ""
                    txtSignMobile.text = ""
                    txtSignPass.text = ""
                    strIMagename = ""
                    imgProfile.image = UIImage(named: "dummy_user")
                   // imgProfile.contentMode = .center
                    btnback.isHidden = false
        self.view.tag = 2
        switchViewAccordingCOnditions()
    }
    @IBAction func actionONBackLogin(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)

           self.view.tag = 1
           switchViewAccordingCOnditions()
       }
    @IBAction func actionONForgotPass(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)

        self.view.endEditing(true)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "ForgotPassVC") as? ForgotPassVC
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func actionOnShowHidePass(_ sender: UIButton) {
        self.view.endEditing(true)
        scaleAnimationONUIButton(sender: sender)

        if(sender.currentImage == UIImage(named: "uncheck")){
            sender.setImage(UIImage(named: "check"), for: .normal)
            txtPassword.isSecureTextEntry = true
            
        }else{
            sender.setImage(UIImage(named: "uncheck"), for: .normal)
            txtPassword.isSecureTextEntry = false
        }
    }
    
    @IBAction func actionONRemember(_ sender: UIButton) {
        self.view.endEditing(true)
                      if(sender.currentImage == UIImage(named: "uncheck")){
                          sender.setImage(UIImage(named: "check"), for: .normal)
                      }else{
                          sender.setImage(UIImage(named: "uncheck"), for: .normal)
                      }
    }
    
    @IBAction func actionONlogin(_ sender: UIButton) {
        self.view.endEditing(true)
        scaleAnimationONUIButton(sender: sender)
        if(validationForLOGIN()){
            self.LoginAPI()
        }
    }
    
    
    //MARK: --------------Action SIGNUP view------------
    
    @IBAction func actionOnSignup(_ sender: UIButton) {
        self.view.endEditing(true)
        scaleAnimationONUIButton(sender: sender)

        if(validationForSIGNUP()){
            self.SignUPAPI()
        }
    }
    
    @IBAction func actionOnBROWSE(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: "", message: "Make your selection", preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor.black
        let camera = (UIAlertAction(title: alert_CAMERA, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertCalling, viewcontrol: self)

                
            }
        }))
        camera.setValue(#imageLiteral(resourceName: "002-camera"), forKey: "image")
        camera.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(camera)
        
        let Gallery = (UIAlertAction(title: alert_Gallery, style: .default , handler:{ (UIAlertAction)in
            self.imagePicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }else{
                showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertCalling, viewcontrol: self)

                
            }
            
        }))
        
        Gallery.setValue(#imageLiteral(resourceName: "001-gallery"), forKey: "image")
        Gallery.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        alert.addAction(Gallery)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Add Cancel  button")
        }))
        
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = sender as UIView
            popoverController.sourceRect = sender.bounds
            popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
        }
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
    }
    
    
    
    
    
}
// MARK:
// MARK:- -------------UITextFieldDelegate----------

extension LoginSigUpVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing( true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtMobileNumber || textField == txtSignMobile){
            return TextValidation(textField: textField, string: string, returnOnly: "NUMBER", limitValue: 9)
        }
        else {
            return TextValidation(textField: textField, string: string, returnOnly: "", limitValue: 45)
        }
    }
}
// MARK:
// MARK:- Validation

extension LoginSigUpVC {
    func validationForLOGIN() -> Bool {
        
        if(txtMobileNumber.text?.count == 0){
            addErrorMessage(textView: txtMobileNumber, message: alert_MobileNumber)
            
            return false
        }else if(txtMobileNumber.text!.count < 10){
            
            addErrorMessage(textView: txtMobileNumber, message: alert_MobileNumberValid)
            return false
            
        }else if(txtPassword.text!.count == 0){
            addErrorMessage(textView: txtPassword, message: alert_Password)
            return false
        }
        return true
    }
    func validationForSIGNUP() -> Bool {
        if(txtSignUserName.text?.count == 0){
            addErrorMessage(textView: txtSignUserName, message: alert_UserName)
            
            return false
        }
        else if(txtSignEmail.text?.count == 0){
            addErrorMessage(textView: txtSignEmail, message: alert_Email)
            
            return false
        }
        else if !(txtSignEmail.text!.isValidEmailAddress()){
            addErrorMessage(textView: txtSignEmail, message: alert_EmailValid)
            
            return false
        }
        else if(txtSignMobile.text?.count == 0){
            addErrorMessage(textView: txtSignMobile, message: alert_MobileNumber)
            
            return false
        }else if(txtSignMobile.text!.count < 10){
            
            addErrorMessage(textView: txtSignMobile, message: alert_MobileNumberValid)
            return false
            
        }else if(txtSignPass.text!.count == 0){
            addErrorMessage(textView: txtSignPass, message: alert_Password)
            return false
        }
        return true
    }
    func addErrorMessage(textView : ACFloatingTextfield, message : String) {
        textView.shakeLineWithError = true
        textView.showErrorWithText(errorText: message)
    }
    
}
// MARK: -
// MARK: - API Calling

extension LoginSigUpVC  {
    func LoginAPI() {
        if(isInternetAvailable()){
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(txtMobileNumber.text!)", forKey: "u_phone")
            dictdata.setValue("\(txtPassword.text!)", forKey: "u_pass")
            dictdata.setValue(str_DeviceID, forKey: "device_id")
            dictdata.setValue(fcm_token, forKey: "device_token")
            dictdata.setValue(platform, forKey: "platform")
            
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_Login) { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                        if (status){
                            let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                            print(dict)
                            if("\(dict.value(forKey: "error")!)" == "1"){
                               showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                            }else{
                                let loginData = (dict.value(forKey: "response")as! NSDictionary).mutableCopy()as! NSMutableDictionary

                                loginData.setValue(self.btnRemember.currentImage == UIImage(named: "check") ? "true" : "false", forKey: "Remember")
                                loginData.setValue("\(dict.value(forKey: "role")!)", forKey: "role")
                                loginData.setValue("\(self.txtPassword.text!)", forKey: "password")

                                
                                if("\(loginData.value(forKey: "otp_verified")!)" == "0"){
                                    self.view.endEditing(true)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPVerificationVC") as? OTPVerificationVC
                                    vc!.strComeFrom = "Login"
                                    vc!.dictDAta = loginData
                                    self.navigationController?.pushViewController(vc!, animated: true)
                                }else{
                                    nsud.setValue(loginData, forKey: "Salon_LoginData")
                                    nsud.synchronize()
                                    self.view.endEditing(true)
                                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                                    self.navigationController?.pushViewController(vc!, animated: true)
                                }
                            }
                        }else{
                            showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)
                        }
               }
        }
        
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)

        }
    }
    func SignUPAPI() {
        if(isInternetAvailable()){
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(txtSignUserName.text!)", forKey: "full_name")
            dictdata.setValue("\(txtSignMobile.text!)", forKey: "user_phone")
            dictdata.setValue("\(txtSignPass.text!)", forKey: "user_pass")
            dictdata.setValue("\(txtSignEmail.text!)", forKey: "user_email")
            dictdata.setValue(str_DeviceID, forKey: "device_id")
            dictdata.setValue(fcm_token, forKey: "device_token")
            dictdata.setValue(platform, forKey: "platform")
            dictdata.setValue("", forKey: "signup_via")
            dictdata.setValue(strIMagename, forKey: "photo")

            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)
            
            WebService.callAPIWithImage(parameter: dictdata, url: URL_register, image: self.imgProfile.image!, fileName: strIMagename, withName: strIMagename) { (responce, status) in
                loader.dismiss(animated: false, completion: nil)
                if (status){
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary
                    print(dict)
                    if("\(dict.value(forKey: "error")!)" == "1"){
                        showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        showToast(message: "\(dict.value(forKey: "message")!)", font: UIFont.systemFont(ofSize: 10), viewww: self.view)
                        self.view.endEditing(true)
                        let loginData = (dict.value(forKey: "response")as! NSDictionary).mutableCopy()as! NSMutableDictionary

                        loginData.setValue(self.btnRemember.currentImage == UIImage(named: "") ? "true" : "false", forKey: "Remember")
                        loginData.setValue("\(dict.value(forKey: "role")!)", forKey: "role")
                        loginData.setValue("\(self.txtSignPass.text!)", forKey: "password")

                        let vc = mainStoryboard.instantiateViewController(withIdentifier: "OTPVerificationVC") as? OTPVerificationVC
                        vc!.strComeFrom = "Login"
                      
                        vc!.dictDAta = loginData
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                    
                }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                    
                }
            }
   
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)

        }
    }
}

// MARK: -
// MARK: ---------UIImagePickerControllerDelegate------
extension LoginSigUpVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        self.imgProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        self.imgProfile.contentMode = .scaleAspectFill
        strIMagename = getUniqueString() + "iOSProfile"
    }
}

//
//  HistoryDetailVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/9/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class HistoryDetailVC: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblServiceNAme: UILabel!
    @IBOutlet weak var lblServiceAmount: UILabel!
    @IBOutlet weak var lblDiscountAmount: UILabel!
    @IBOutlet weak var lblConveynceFeeAmount: UILabel!
   
    var aryList = NSMutableArray()
    var refreshControl = UIRefreshControl()
    var dictDATA = NSMutableDictionary()

    // MARK: - ------LifeCycle----------
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tv_List.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tv_List.addSubview(refreshControl)
        print(dictDATA)
        lblTotalAmount.text = "\(dictDATA.value(forKey: "order_total")!)" == "" ? "Total Amount - \u{20B9}0.0" : "Total Amount - \u{20B9}\(dictDATA.value(forKey: "order_total")!)"
        
        lblConveynceFeeAmount.text = "\(dictDATA.value(forKey: "convince_fee")!)" == "" ? "Conveynce Fee - \u{20B9}0.0" : "Conveynce Fee - \u{20B9}\(dictDATA.value(forKey: "convince_fee")!)"
        
        lblDiscountAmount.text = "\(dictDATA.value(forKey: "discount")!)" == "" ? "Discount - \u{20B9}0.0" : "Discount - \u{20B9}\(dictDATA.value(forKey: "discount")!)"
        let serviceData = dictDATA.value(forKey: "addon_services")as! NSDictionary
        lblServiceNAme.text = "\(serviceData.value(forKey: "service_name")!)" == "" ? " " : "\(serviceData.value(forKey: "service_name")!)"
        lblServiceAmount.text = "\(serviceData.value(forKey: "service_amount")!)" == "" ? "0" : "\(serviceData.value(forKey: "service_amount")!)"
        lblServiceAmount.text = "\u{20B9}\(lblServiceAmount.text!)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true
        if(aryList.count == 0){
             self.HistoryDetailListAPI()
        }else{
            self.tv_List.reloadData()
            self.tv_List.delegate = self
            self.tv_List.dataSource = self
        }
    }
    @objc func refresh(_ sender: AnyObject) {
        self.HistoryDetailListAPI()
    }

    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnBack(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.navigationController?.popViewController(animated: true)
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension HistoryDetailVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "OfferCell" : "OfferCell", for: indexPath as IndexPath) as! OfferCell
        let dict = aryList.object(at: indexPath.row)as! NSDictionary
        cell.lblDisAmmount.text = ""
        cell.lblAmmount.text = "\u{20B9}\(dict.value(forKey: "product_price")!)"
        cell.lblTitle.text =  "\(dict.value(forKey: "product_name")!)"
        
        
        let urlImage = "\(dict.value(forKey: "product_img")!)"
        cell.imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "salon_on_wheels"), options: SDWebImageOptions(rawValue: 0), progress: { (S1, S2) in
        }, completed: { (IMAGE, ERROR, cacheType, imageURL) in
            
        }, usingActivityIndicatorStyle: .gray)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryList.count == 0 ? 100 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
}

// MARK: -
// MARK: - ---------------API Calling----------

extension HistoryDetailVC  {
    func HistoryDetailListAPI() {
        if(isInternetAvailable()){
         self.activityIndicator.isHidden = false
            let dictdata = NSMutableDictionary()
            dictdata.setValue("\(dictDATA.value(forKey: "order_id")!)", forKey: "order_id")

            WebService.callAPIBYPOST(parameter: dictdata, url: URL_get_order_by_id) { (responce, status) in
              self.activityIndicator.isHidden = true
                self.refreshControl.endRefreshing()

                if (status){
                    let dict = (responce as NSDictionary).value(forKey: "data")as! NSDictionary
                    self.aryList = NSMutableArray()
                    if(dict.value(forKey: "order_products") != nil){
                        self.aryList = (dict.value(forKey: "order_products")as! NSArray).mutableCopy() as! NSMutableArray
                        
                    }
                    self.tv_List.reloadData()
                    self.tv_List.delegate = self
                    self.tv_List.dataSource = self
            }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
}

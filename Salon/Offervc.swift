//
//  Offervc.swift
//  Salon
//
//  Created by NavinPatidar on 10/6/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class Offervc: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------
    @IBOutlet weak var tv_List: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var aryList = NSMutableArray()
    var refreshControl = UIRefreshControl()

    // MARK: - ------LifeCycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        tv_List.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Refresh")
         refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
         tv_List.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.activityIndicator.isHidden = true

        if(aryList.count == 0){
             self.OfferAPI()
        }else{
            self.tv_List.reloadData()
            self.tv_List.delegate = self
            self.tv_List.dataSource = self
        }
    }
    @objc func refresh(_ sender: AnyObject) {
        self.OfferAPI()
    }
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnMenu(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        
        let nextVc: DrawerVC = mainStoryboard.instantiateViewController(withIdentifier: "DrawerVC") as! DrawerVC
        nextVc.handleDrawerView = self
        nextVc.modalPresentationStyle = .overCurrentContext
        nextVc.modalTransitionStyle = .crossDissolve
        self.present(nextVc, animated: false, completion: nil)
        
    }
    @IBAction func actionOnBottomButton(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        self.view.endEditing(true)
        var count = 0
        if(sender.tag == 0){ // Service
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DashBoardVC.self) {
                    count = 1
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            if(count == 0){
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
        else if(sender.tag == 1){ //Offer
            
        }
        else if(sender.tag == 2){ //Appoinment
            if(globleLogInData.count == 0){
                let alert = UIAlertController(title: alertMsg, message: alertLogin, preferredStyle: UIAlertController.Style.alert)
                alert.view.tintColor = UIColor.black
                alert.addAction(UIAlertAction (title: "LOGIN", style: .default, handler: { (nil) in
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                    self.navigationController?.pushViewController(vc!, animated: true)
                }))
                alert.addAction(UIAlertAction (title: "CLOSE", style: .default, handler: { (nil) in
                    
                   // self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
            
            
            
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: AppoinmentVC.self) {
                    count = 1
                    self.navigationController!.popToViewController(controller, animated: false)
                    break
                }
            }
            if(count == 0){
                
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AppoinmentVC") as? AppoinmentVC
                self.navigationController?.pushViewController(vc!, animated: false)
            }
                
            }
        }
        else if(sender.tag == 3){ //Profile
            if(globleLogInData.count == 0){
                let alert = UIAlertController(title: alertMsg, message: alertLogin, preferredStyle: UIAlertController.Style.alert)
                alert.view.tintColor = UIColor.black
                alert.addAction(UIAlertAction (title: "LOGIN", style: .default, handler: { (nil) in
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                    self.navigationController?.pushViewController(vc!, animated: true)
                }))
                alert.addAction(UIAlertAction (title: "CLOSE", style: .default, handler: { (nil) in
                    
                  //  self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: MyProfileVC.self) {
                        count = 1
                        
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                if(count == 0){
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
            }
          
        }
        
    }
}
// MARK: - ----------------UITableViewDelegate
// MARK: -

extension Offervc: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tv_List.dequeueReusableCell(withIdentifier: DeviceType.IS_IPAD ? "OfferCell" : "OfferCell", for: indexPath as IndexPath) as! OfferCell
        let dict = ((aryList.object(at: indexPath.row)as! NSDictionary).mutableCopy()as! NSMutableDictionary).removeNullFromDict()
        cell.lblTitle.text = "\(dict.value(forKey: "title")!)"
        cell.lblDisAmmount.text = "\u{20B9}\(dict.value(forKey: "regular_price")!)"
        cell.lblAmmount.text = "\u{20B9}\(dict.value(forKey: "price")!)"
    
        if("\(dict.value(forKey: "regular_price")!)" == ""){
            cell.lblDisAmmount.text = ""
        }
        if("\(dict.value(forKey: "regular_price")!)" == "\(dict.value(forKey: "price")!)"){
            cell.lblDisAmmount.text = ""
        }
        
        
        
        cell.imgView.image = UIImage(named: "salon_on_wheels")
        cell.imgView.layer.cornerRadius =  6.0
        cell.imgView.layer.masksToBounds = false
        cell.imgView.clipsToBounds = true
        if(dict.value(forKey: "images") is NSArray){
            let aryImage = dict.value(forKey: "images") as! NSArray
            if(aryImage.count != 0){
                let urlImage = "\(aryImage.object(at: 0))"
                cell.imgView.setImageWith(URL(string: urlImage), placeholderImage: UIImage(named: "salon_on_wheels"), options: SDWebImageOptions(rawValue: 0), progress: { (S1, S2) in
                }, completed: { (IMAGE, ERROR, cacheType, imageURL) in
                   
                }, usingActivityIndicatorStyle: .gray)
            }
        }
     
      
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
            let dict = aryList.object(at: indexPath.row)as! NSDictionary
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "ServiceDetailVC") as? ServiceDetailVC
            vc?.dictData = dict.mutableCopy()as! NSMutableDictionary
            self.navigationController?.pushViewController(vc!, animated: true)
      

    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return aryList.count == 0 ? 100 : 0
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100))
        let lbl = UILabel.init(frame: CGRect(x: 5, y: 0, width: tableView.frame.width - 10, height: 100))
        lbl.text = aryList.count == 0 ? alertDataNotFound : ""
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        view.addSubview(lbl)
        return view
    }
  
}

// MARK: -
// MARK: ----------OfferCell---------
class OfferCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAmmount: UILabel!
    @IBOutlet weak var lblDisAmmount: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    
    @IBOutlet weak var lblTotalamount: UILabel!
    @IBOutlet weak var lblDisamount: UILabel!
    @IBOutlet weak var lblConvenFee: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var btnBook: UIButton!
    @IBOutlet weak var btnPromocode: UIButton!
    @IBOutlet weak var btnADDNOW: UIButton!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
// MARK: -
// MARK: - ---------------API Calling----------

extension Offervc  {
    func OfferAPI() {
        if(isInternetAvailable()){
         self.activityIndicator.isHidden = false
            let dictdata = NSMutableDictionary()
            dictdata.setValue("true", forKey: "on_sale")
   
            WebService.callAPIBYPOST(parameter: dictdata, url: URL_get_products) { (responce, status) in
              self.activityIndicator.isHidden = true
                self.refreshControl.endRefreshing()
                print(responce)
                if (status){
                    if((responce as NSDictionary).value(forKey: "data") is NSArray){
                        let dict = (responce as NSDictionary).value(forKey: "data")as! NSArray
                        self.aryList = NSMutableArray()
                        self.aryList = dict.mutableCopy() as! NSMutableArray
                        self.tv_List.reloadData()
                        self.tv_List.delegate = self
                        self.tv_List.dataSource = self
                    }
                }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)
                }
            }
        }
        else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
        }
    }
    func LogOutAPI() {
         if(isInternetAvailable()){
             let dictdata = NSMutableDictionary()
            let loader = loader_Show(controller: self, strMessage: "Please wait...", title: "", style: .alert)

            dictdata.setValue("\(globleLogInData.value(forKey: "user_id")!)", forKey: "user_id")
             WebService.callAPIBYPOST(parameter: dictdata, url: URL_LogOut) { (responce, status) in
                 if (status){
                    loader.dismiss(animated: false, completion: nil)
                    let dict = ((responce as NSDictionary).value(forKey: "data")as! NSDictionary).mutableCopy()as! NSMutableDictionary

                    
                    if("\(dict.value(forKey: "error")!)" == "1"){
                        showAlertWithoutAnyAction(strtitle: "Failed", strMessage: "\(dict.value(forKey: "message")!)", viewcontrol: self)
                    }else{
                        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
                        UserDefaults.standard.synchronize()
                        globleLogInData = NSMutableDictionary()

                        self.view.endEditing(true)
                        let redViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as! LoginSigUpVC
                        let navigationController = UINavigationController(rootViewController: redViewController)

                        UIApplication.shared.windows.first?.rootViewController = navigationController
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
                        navigationController.setNavigationBarHidden(true, animated: true)
                    }
                    
                 }else{
                    showAlertWithoutAnyAction(strtitle: "Error", strMessage: (responce as NSDictionary).value(forKey: "message")as! String, viewcontrol: self)

                 }
             }
         }
         else{
            showAlertWithoutAnyAction(strtitle: "Error", strMessage: alertInternet, viewcontrol: self)
         }
     }
}

// MARK: - ------------DrawerScreenDelegate---------
// MARK: -
extension Offervc: DrawerScreenDelegate {
    func refreshDrawerScreen(strType: String, tag: Int) {
            if(strType == "Sign out"){
                       self.view.endEditing(true)
                       let alert = UIAlertController(title: "Sign out", message: alertLogout, preferredStyle: .alert)
                       alert.view.tintColor = UIColor.black
                       let Yes = (UIAlertAction(title: "Yes", style: .default , handler:{ (UIAlertAction)in
                       self.LogOutAPI()
                       }))
                       alert.addAction(Yes)
                   
                       alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                           print("User click Add Cancel  button")
                       }))
                       
                       
                       if let popoverController = alert.popoverPresentationController {
                           popoverController.sourceView = self.view
                           popoverController.sourceRect = self.view.bounds
                           popoverController.permittedArrowDirections = UIPopoverArrowDirection.down
                       }
                       
                       self.present(alert, animated: true, completion: {
                           print("completion block")
                       })
                       
                   }
                   
                   
                   else if (strType == "Change Password"){
                       self.view.endEditing(true)
                           let vc = mainStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as? ChangePasswordVC
                           self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Feedback"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "FeedBackVC") as? FeedBackVC
                self.navigationController?.pushViewController(vc!, animated: true)
        }
        else if (strType == "About Us"){
                      self.view.endEditing(true)
                      let vc = mainStoryboard.instantiateViewController(withIdentifier: "AboutUSVC") as? AboutUSVC
                      self.navigationController?.pushViewController(vc!, animated: true)
              }
        else if (strType == "Contact Us"){
                      self.view.endEditing(true)
                      let vc = mainStoryboard.instantiateViewController(withIdentifier: "ContactUSVC") as? ContactUSVC
                      self.navigationController?.pushViewController(vc!, animated: true)
              }
        else if (strType == "Assigned Booking List"){
                            self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AssignedBookingListVC") as? AssignedBookingListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Glimpses of Gallery"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "GlimpsesofGalleryVC") as? GlimpsesofGalleryVC
                self.navigationController?.pushViewController(vc!, animated: true)
        }
            else if (strType == "History"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "HistoryVC") as? HistoryVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }  else if (strType == "Review"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "ReviewListVC") as? ReviewListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }  else if (strType == "Appointment List"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "AppointmentListVC") as? AppointmentListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Staff List"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "StaffListVC") as? StaffListVC
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            else if (strType == "Profile"){ //Profile
                var count = 0
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: MyProfileVC.self) {
                        count = 1
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                if(count == 0){
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "MyProfileVC") as? MyProfileVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
            }
            else if (strType == "Home"){
                self.view.endEditing(true)
                var count = 0
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: DashBoardVC.self) {
                        count = 1
                        self.navigationController!.popToViewController(controller, animated: false)
                        break
                    }
                }
                if(count == 0){
                    
                    let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
                    self.navigationController?.pushViewController(vc!, animated: false)
                }
         
            }
            else if (strType == "Login"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                vc!.view.tag = 1

                self.navigationController?.pushViewController(vc!, animated: false)
         
            }
            else if (strType == "Signup"){
                self.view.endEditing(true)
                let vc = mainStoryboard.instantiateViewController(withIdentifier: "LoginSigUpVC") as? LoginSigUpVC
                vc!.view.tag = 2

                self.navigationController?.pushViewController(vc!, animated: false)
         
            }
    }
    
    
    
}

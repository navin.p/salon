//
//  ThankYouVC.swift
//  Salon
//
//  Created by NavinPatidar on 10/8/20.
//  Copyright © 2020 Salon. All rights reserved.
//

import UIKit

class ThankYouVC: UIViewController {
    //MARK:
    //MARK: ---------IBOutlet------------
    var dictDetail = NSMutableDictionary()
    
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var imgRIght: UIImageView!

    
    // MARK: - ------LifeCycle----------
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
            lblOrderID.attributedText = attributedText(withString: "Booking Number \(dictDetail.value(forKey: "number")!)", boldString: "\(dictDetail.value(forKey: "number")!)", font: lblOrderID.font)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        imgRIght.transform = CGAffineTransform.init(scaleX: 0.6, y: 0.6)
            UIView.animate(withDuration: 0.4, animations: { () -> Void in
                self.imgRIght.transform = CGAffineTransform.init(scaleX: 1, y: 1)
            })
    }
    
    // MARK: - ------IBAction----------
    // MARK: -
    @IBAction func actionOnContinue(_ sender: UIButton) {
        scaleAnimationONUIButton(sender: sender)
        var count = 0
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashBoardVC.self) {
                count = 1
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
        if(count == 0){
            
            let vc = mainStoryboard.instantiateViewController(withIdentifier: "DashBoardVC") as? DashBoardVC
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                     attributes: [NSAttributedString.Key.font: font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }

}
